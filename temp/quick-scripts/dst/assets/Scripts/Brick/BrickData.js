
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Brick/BrickData.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c0611MLpDRDp4LPc1DY52go', 'BrickData');
// Scripts/Brick/BrickData.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayController_1 = require("../Common/GamePlayController");
var BrickData = /** @class */ (function (_super) {
    __extends(BrickData, _super);
    function BrickData() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.mesh = null;
        return _this;
    }
    BrickData.prototype.setmaterial = function (materialType) {
        var index = 0;
        if (materialType == EnumDefine_1.BrickType.BrickCharacter)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickCharacter);
        else if (materialType == EnumDefine_1.BrickType.BrickAI1)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickAI[0]);
        else if (materialType == EnumDefine_1.BrickType.BrickAI2)
            this.mesh.setMaterial(0, GamePlayController_1.default.Instance(GamePlayController_1.default).materialBrickAI[1]);
    };
    __decorate([
        property(cc.MeshRenderer)
    ], BrickData.prototype, "mesh", void 0);
    BrickData = __decorate([
        ccclass
    ], BrickData);
    return BrickData;
}(cc.Component));
exports.default = BrickData;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnJpY2tcXEJyaWNrRGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBQzVDLG1EQUFpRDtBQUNqRCxtRUFBOEQ7QUFFOUQ7SUFBdUMsNkJBQVk7SUFEbkQ7UUFBQSxxRUFjQztRQVhHLFVBQUksR0FBb0IsSUFBSSxDQUFDOztJQVdqQyxDQUFDO0lBVkcsK0JBQVcsR0FBWCxVQUFZLFlBQXVCO1FBQy9CLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLElBQUksWUFBWSxJQUFJLHNCQUFTLENBQUMsY0FBYztZQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsNEJBQWtCLENBQUMsUUFBUSxDQUFDLDRCQUFrQixDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQzthQUNoRyxJQUFJLFlBQVksSUFBSSxzQkFBUyxDQUFDLFFBQVE7WUFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDRCQUFrQixDQUFDLFFBQVEsQ0FBQyw0QkFBa0IsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzVGLElBQUksWUFBWSxJQUFJLHNCQUFTLENBQUMsUUFBUTtZQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsNEJBQWtCLENBQUMsUUFBUSxDQUFDLDRCQUFrQixDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDckcsQ0FBQztJQVREO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUM7MkNBQ0c7SUFGWixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBYTdCO0lBQUQsZ0JBQUM7Q0FiRCxBQWFDLENBYnNDLEVBQUUsQ0FBQyxTQUFTLEdBYWxEO2tCQWJvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuaW1wb3J0IHsgQnJpY2tUeXBlIH0gZnJvbSBcIi4uL0NvbW1vbi9FbnVtRGVmaW5lXCI7XHJcbmltcG9ydCBHYW1lUGxheUNvbnRyb2xsZXIgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUNvbnRyb2xsZXJcIjtcclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnJpY2tEYXRhIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eShjYy5NZXNoUmVuZGVyZXIpXHJcbiAgICBtZXNoOiBjYy5NZXNoUmVuZGVyZXIgPSBudWxsO1xyXG4gICAgc2V0bWF0ZXJpYWwobWF0ZXJpYWxUeXBlOiBCcmlja1R5cGUpIHtcclxuICAgICAgICBsZXQgaW5kZXggPSAwO1xyXG4gICAgICAgIGlmIChtYXRlcmlhbFR5cGUgPT0gQnJpY2tUeXBlLkJyaWNrQ2hhcmFjdGVyKVxyXG4gICAgICAgICAgICB0aGlzLm1lc2guc2V0TWF0ZXJpYWwoMCwgR2FtZVBsYXlDb250cm9sbGVyLkluc3RhbmNlKEdhbWVQbGF5Q29udHJvbGxlcikubWF0ZXJpYWxCcmlja0NoYXJhY3Rlcik7XHJcbiAgICAgICAgZWxzZSBpZiAobWF0ZXJpYWxUeXBlID09IEJyaWNrVHlwZS5Ccmlja0FJMSlcclxuICAgICAgICAgICAgdGhpcy5tZXNoLnNldE1hdGVyaWFsKDAsIEdhbWVQbGF5Q29udHJvbGxlci5JbnN0YW5jZShHYW1lUGxheUNvbnRyb2xsZXIpLm1hdGVyaWFsQnJpY2tBSVswXSk7XHJcbiAgICAgICAgZWxzZSBpZiAobWF0ZXJpYWxUeXBlID09IEJyaWNrVHlwZS5Ccmlja0FJMilcclxuICAgICAgICAgICAgdGhpcy5tZXNoLnNldE1hdGVyaWFsKDAsIEdhbWVQbGF5Q29udHJvbGxlci5JbnN0YW5jZShHYW1lUGxheUNvbnRyb2xsZXIpLm1hdGVyaWFsQnJpY2tBSVsxXSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==