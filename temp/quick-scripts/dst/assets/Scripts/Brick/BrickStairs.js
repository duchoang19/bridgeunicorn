
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Brick/BrickStairs.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6e770L2b9VC3KZmrEF0oM/r', 'BrickStairs');
// Scripts/Brick/BrickStairs.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("../Controller/CharacterController");
var BrickData_1 = require("./BrickData");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BrickStairs = /** @class */ (function (_super) {
    __extends(BrickStairs, _super);
    function BrickStairs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.boolAppeared = false;
        return _this;
    }
    BrickStairs.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    };
    BrickStairs.prototype.onTriggerEnter = function (event) {
        if (event.otherCollider.node.group == "Player") {
            if (!this.boolAppeared) {
                cc.audioEngine.playEffect(Global_1.default.soundUpStairs, false);
                GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeOut(0.3));
                this.boolAppeared = true;
                this.node.opacity = 255;
                this.node.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
                event.otherCollider.node.getComponent(CharacterController_1.default).DecreaseStick();
                event.otherCollider.node.getComponent(CharacterController_1.default).DecreaseLevel();
            }
        }
    };
    BrickStairs = __decorate([
        ccclass
    ], BrickStairs);
    return BrickStairs;
}(cc.Component));
exports.default = BrickStairs;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQnJpY2tcXEJyaWNrU3RhaXJzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG1EQUFpRDtBQUVqRCwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBQ3RDLHlFQUFvRTtBQUNwRSx5Q0FBb0M7QUFFOUIsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUF5QywrQkFBWTtJQURyRDtRQUFBLHFFQW9CQztRQWxCRyxrQkFBWSxHQUFZLEtBQUssQ0FBQzs7SUFrQmxDLENBQUM7SUFqQkcsMkJBQUssR0FBTDtRQUNJLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUNELG9DQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2hCLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLFFBQVEsRUFBRTtZQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDcEIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxhQUFhLEVBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RELDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDekYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxzQkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN4RSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDM0UsS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDOUU7U0FDSjtJQUNMLENBQUM7SUFsQmdCLFdBQVc7UUFEL0IsT0FBTztPQUNhLFdBQVcsQ0FtQi9CO0lBQUQsa0JBQUM7Q0FuQkQsQUFtQkMsQ0FuQndDLEVBQUUsQ0FBQyxTQUFTLEdBbUJwRDtrQkFuQm9CLFdBQVciLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCcmlja1R5cGUgfSBmcm9tIFwiLi4vQ29tbW9uL0VudW1EZWZpbmVcIjtcclxuaW1wb3J0IEdhbWVQbGF5Q29udHJvbGxlciBmcm9tIFwiLi4vQ29tbW9uL0dhbWVQbGF5Q29udHJvbGxlclwiO1xyXG5pbXBvcnQgR2FtZVBsYXlJbnN0YW5jZSBmcm9tIFwiLi4vQ29tbW9uL0dhbWVQbGF5SW5zdGFuY2VcIjtcclxuaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5pbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vQ29udHJvbGxlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCBCcmlja0RhdGEgZnJvbSBcIi4vQnJpY2tEYXRhXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnJpY2tTdGFpcnMgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgYm9vbEFwcGVhcmVkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICBsZXQgY29sbGlkZXIgPSB0aGlzLmdldENvbXBvbmVudChjYy5Db2xsaWRlcjNEKTtcclxuICAgICAgICBjb2xsaWRlci5vbigndHJpZ2dlci1lbnRlcicsIHRoaXMub25UcmlnZ2VyRW50ZXIsIHRoaXMpO1xyXG4gICAgfVxyXG4gICAgb25UcmlnZ2VyRW50ZXIoZXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLmdyb3VwID09IFwiUGxheWVyXCIpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmJvb2xBcHBlYXJlZCkge1xyXG4gICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRVcFN0YWlycyxmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LnR4dENsaW1iLnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMykpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ib29sQXBwZWFyZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KEJyaWNrRGF0YSkuc2V0bWF0ZXJpYWwoQnJpY2tUeXBlLkJyaWNrQ2hhcmFjdGVyKTtcclxuICAgICAgICAgICAgICAgIGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuRGVjcmVhc2VTdGljaygpO1xyXG4gICAgICAgICAgICAgICAgZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5EZWNyZWFzZUxldmVsKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19