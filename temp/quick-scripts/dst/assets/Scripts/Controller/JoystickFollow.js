
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/JoystickFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f6068GFEGlHn7TrO7hQLo8v', 'JoystickFollow');
// Scripts/Controller/JoystickFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("./CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var JoysticFollow = /** @class */ (function (_super) {
    __extends(JoysticFollow, _super);
    function JoysticFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.joyRing = null;
        _this.joyDot = null;
        _this.stickPos = null;
        _this.touchLocation = null;
        _this.radius = 0;
        return _this;
    }
    JoysticFollow.prototype.onLoad = function () {
        this.radius = this.joyRing.width / 2;
    };
    JoysticFollow.prototype.start = function () {
        this.gamePlayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.node.on(cc.Node.EventType.TOUCH_START, this.TouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.TouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.TouchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.TouchCancel, this);
    };
    JoysticFollow.prototype.TouchStart = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            var mousePosition = event.getLocation();
            var localMousePosition = this.node.convertToNodeSpaceAR(mousePosition);
            this.node.opacity = 255;
            this.stickPos = localMousePosition;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePosition);
            this.joyDot.setPosition(localMousePosition);
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
            // this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController).ArrowDirection.active = true;
        }
    };
    JoysticFollow.prototype.TouchMove = function (event) {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.node.opacity = 255;
            Global_1.default.boolEnableTouch = true;
            if (!Global_1.default.boolFirstTouchJoyStick) {
                Global_1.default.boolFirstTouchJoyStick = true;
                this.gamePlayInstance.gameplay.myCharacter.getComponent(cc.SkeletonAnimation).play("Run");
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gamePlayInstance.gameplay.Guide.active = false;
            var touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            var distance = touchPos.mag();
            var posX = this.stickPos.x + touchPos.x;
            var posY = this.stickPos.y + touchPos.y;
            var p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global_1.default.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            }
            else {
                var x = this.stickPos.x + p.x * this.radius;
                var y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
            // this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController).ArrowDirection.active = true;
            this.gamePlayInstance.gameplay.Guide.active = false;
            this.gamePlayInstance.gameplay.txtCollectBlock.active = false;
        }
    };
    JoysticFollow.prototype.TouchCancel = function () {
        if (!Global_1.default.boolEndGame && Global_1.default.boolStartPlay && !Global_1.default.boolCharacterFall) {
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).Attacking();
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).StopMove();
            Global_1.default.boolEnableTouch = false;
            this.node.opacity = 0;
            this.gamePlayInstance.gameplay.myCharacter.getComponent(CharacterController_1.default).boolCheckTypeRotate = true;
        }
    };
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyRing", void 0);
    __decorate([
        property(cc.Node)
    ], JoysticFollow.prototype, "joyDot", void 0);
    JoysticFollow = __decorate([
        ccclass
    ], JoysticFollow);
    return JoysticFollow;
}(cc.Component));
exports.default = JoysticFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcSm95c3RpY2tGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsK0RBQTBEO0FBQzFELDJDQUFzQztBQUN0Qyw2REFBd0Q7QUFDbEQsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUU1QztJQUEyQyxpQ0FBWTtJQUR2RDtRQUFBLHFFQTBFQztRQXZFRyxhQUFPLEdBQVksSUFBSSxDQUFDO1FBRXhCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFDdkIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUN6QixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUM5QixZQUFNLEdBQVcsQ0FBQyxDQUFDOztJQWtFdkIsQ0FBQztJQWhFRyw4QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUNELDZCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUNELGtDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ1osSUFBSSxDQUFDLGdCQUFNLENBQUMsV0FBVyxJQUFJLGdCQUFNLENBQUMsYUFBYSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxpQkFBaUIsRUFBRTtZQUMxRSxJQUFJLGFBQWEsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDeEMsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLGtCQUFrQixDQUFDO1lBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDOUQsNkdBQTZHO1NBQ2hIO0lBQ0wsQ0FBQztJQUNELGlDQUFTLEdBQVQsVUFBVSxLQUFLO1FBQ1gsSUFBSSxDQUFDLGdCQUFNLENBQUMsV0FBVyxJQUFJLGdCQUFNLENBQUMsYUFBYSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxpQkFBaUIsRUFBRTtZQUMxRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7WUFDeEIsZ0JBQU0sQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQzlCLElBQUksQ0FBQyxnQkFBTSxDQUFDLHNCQUFzQixFQUFFO2dCQUNoQyxnQkFBTSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztnQkFDckMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM3RjtZQUNELElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxLQUFLLENBQUMsV0FBVyxFQUFFLEVBQUU7Z0JBQzVDLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQ3RFLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUM5QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN0RSxnQkFBTSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDcEIsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUM5QztpQkFBTTtnQkFDSCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4QztZQUNELDZHQUE2RztZQUM3RyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDakU7SUFDTCxDQUFDO0lBQ0QsbUNBQVcsR0FBWDtRQUNJLElBQUksQ0FBQyxnQkFBTSxDQUFDLFdBQVcsSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsaUJBQWlCLEVBQUU7WUFDMUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3pGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hGLGdCQUFNLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1NBQzNHO0lBQ0wsQ0FBQztJQXRFRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNNO0lBRXhCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ0s7SUFKTixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBeUVqQztJQUFELG9CQUFDO0NBekVELEFBeUVDLENBekUwQyxFQUFFLENBQUMsU0FBUyxHQXlFdEQ7a0JBekVvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4vQ2hhcmFjdGVyQ29udHJvbGxlclwiO1xyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBKb3lzdGljRm9sbG93IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgam95UmluZzogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveURvdDogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBzdGlja1BvczogY2MuVmVjMiA9IG51bGw7XHJcbiAgICB0b3VjaExvY2F0aW9uOiBjYy5WZWMyID0gbnVsbDtcclxuICAgIHJhZGl1czogbnVtYmVyID0gMDtcclxuICAgIGdhbWVQbGF5SW5zdGFuY2U6IEdhbWVQbGF5SW5zdGFuY2U7XHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgdGhpcy5yYWRpdXMgPSB0aGlzLmpveVJpbmcud2lkdGggLyAyO1xyXG4gICAgfVxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lUGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIHRoaXMuVG91Y2hTdGFydCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX01PVkUsIHRoaXMuVG91Y2hNb3ZlLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCB0aGlzLlRvdWNoQ2FuY2VsLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfQ0FOQ0VMLCB0aGlzLlRvdWNoQ2FuY2VsLCB0aGlzKTtcclxuICAgIH1cclxuICAgIFRvdWNoU3RhcnQoZXZlbnQpIHtcclxuICAgICAgICBpZiAoIUdsb2JhbC5ib29sRW5kR2FtZSAmJiBHbG9iYWwuYm9vbFN0YXJ0UGxheSAmJiAhR2xvYmFsLmJvb2xDaGFyYWN0ZXJGYWxsKSB7XHJcbiAgICAgICAgICAgIHZhciBtb3VzZVBvc2l0aW9uID0gZXZlbnQuZ2V0TG9jYXRpb24oKTtcclxuICAgICAgICAgICAgbGV0IGxvY2FsTW91c2VQb3NpdGlvbiA9IHRoaXMubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihtb3VzZVBvc2l0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgICAgIHRoaXMuc3RpY2tQb3MgPSBsb2NhbE1vdXNlUG9zaXRpb247XHJcbiAgICAgICAgICAgIHRoaXMudG91Y2hMb2NhdGlvbiA9IGV2ZW50LmdldExvY2F0aW9uKCk7XHJcbiAgICAgICAgICAgIHRoaXMuam95UmluZy5zZXRQb3NpdGlvbihsb2NhbE1vdXNlUG9zaXRpb24pO1xyXG4gICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbihsb2NhbE1vdXNlUG9zaXRpb24pO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS50eHRDb2xsZWN0QmxvY2suYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuQXJyb3dEaXJlY3Rpb24uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBUb3VjaE1vdmUoZXZlbnQpIHtcclxuICAgICAgICBpZiAoIUdsb2JhbC5ib29sRW5kR2FtZSAmJiBHbG9iYWwuYm9vbFN0YXJ0UGxheSAmJiAhR2xvYmFsLmJvb2xDaGFyYWN0ZXJGYWxsKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbEVuYWJsZVRvdWNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgaWYgKCFHbG9iYWwuYm9vbEZpcnN0VG91Y2hKb3lTdGljaykge1xyXG4gICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xGaXJzdFRvdWNoSm95U3RpY2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nYW1lUGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIlJ1blwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy50b3VjaExvY2F0aW9uID09PSBldmVudC5nZXRMb2NhdGlvbigpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5nYW1lUGxheUluc3RhbmNlLmdhbWVwbGF5Lkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICBsZXQgdG91Y2hQb3MgPSB0aGlzLmpveVJpbmcuY29udmVydFRvTm9kZVNwYWNlQVIoZXZlbnQuZ2V0TG9jYXRpb24oKSk7XHJcbiAgICAgICAgICAgIGxldCBkaXN0YW5jZSA9IHRvdWNoUG9zLm1hZygpO1xyXG4gICAgICAgICAgICBsZXQgcG9zWCA9IHRoaXMuc3RpY2tQb3MueCArIHRvdWNoUG9zLng7XHJcbiAgICAgICAgICAgIGxldCBwb3NZID0gdGhpcy5zdGlja1Bvcy55ICsgdG91Y2hQb3MueTtcclxuICAgICAgICAgICAgbGV0IHAgPSBjYy52Mihwb3NYLCBwb3NZKS5zdWIodGhpcy5qb3lSaW5nLmdldFBvc2l0aW9uKCkpLm5vcm1hbGl6ZSgpO1xyXG4gICAgICAgICAgICBHbG9iYWwudG91Y2hQb3MgPSBwO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5yYWRpdXMgPiBkaXN0YW5jZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24oY2MudjIocG9zWCwgcG9zWSkpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IHggPSB0aGlzLnN0aWNrUG9zLnggKyBwLnggKiB0aGlzLnJhZGl1cztcclxuICAgICAgICAgICAgICAgIGxldCB5ID0gdGhpcy5zdGlja1Bvcy55ICsgcC55ICogdGhpcy5yYWRpdXM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbihjYy52Mih4LCB5KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gdGhpcy5nYW1lUGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVQbGF5SW5zdGFuY2UuZ2FtZXBsYXkuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS50eHRDb2xsZWN0QmxvY2suYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgVG91Y2hDYW5jZWwoKSB7XHJcbiAgICAgICAgaWYgKCFHbG9iYWwuYm9vbEVuZEdhbWUgJiYgR2xvYmFsLmJvb2xTdGFydFBsYXkgJiYgIUdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCkge1xyXG4gICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuQXR0YWNraW5nKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuU3RvcE1vdmUoKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xFbmFibGVUb3VjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZVBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuYm9vbENoZWNrVHlwZVJvdGF0ZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==