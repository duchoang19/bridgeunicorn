
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/ColliderAttack.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ede90aP+upFTbO0wWH/S0jG', 'ColliderAttack');
// Scripts/Controller/ColliderAttack.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var AIController_1 = require("./AIController");
var BoxController_1 = require("./BoxController");
var CharacterController_1 = require("./CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ColliderAttack = /** @class */ (function (_super) {
    __extends(ColliderAttack, _super);
    function ColliderAttack() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.myAI = null;
        return _this;
    }
    ColliderAttack.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    };
    ColliderAttack.prototype.onTriggerEnter = function (event) {
        var _this = this;
        if (event.otherCollider.node.group == "Model2") {
            if (event.otherCollider.node.name == "Box" && !this.myAI.getComponent(AIController_1.default).boolCheckDeath) {
                this.myAI.getComponent(AIController_1.default).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                event.otherCollider.node.getComponent(BoxController_1.default).DieWhenEnemyAttack();
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(cc.SkeletonAnimation).play("Idle");
                }, 0.708);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(AIController_1.default).EnemyRunIdle();
                }, 1.3);
            }
        }
        else if (event.otherCollider.node.group == "Player" && !this.myAI.getComponent(AIController_1.default).boolCheckDeath) {
            var Ran = Utility_1.default.RandomRangeInteger(1, 5);
            if (Ran == 2) {
                this.myAI.getComponent(AIController_1.default).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                event.otherCollider.node.getComponent(CharacterController_1.default).CharacterFall();
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(cc.SkeletonAnimation).play("Idle");
                }, 0.708);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(AIController_1.default).EnemyRunIdle();
                }, 0.85);
            }
        }
    };
    __decorate([
        property(cc.Node)
    ], ColliderAttack.prototype, "myAI", void 0);
    ColliderAttack = __decorate([
        ccclass
    ], ColliderAttack);
    return ColliderAttack;
}(cc.Component));
exports.default = ColliderAttack;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQ29sbGlkZXJBdHRhY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQXNDO0FBQ3RDLDZDQUF3QztBQUN4QywrQ0FBMEM7QUFDMUMsaURBQTRDO0FBQzVDLDZEQUF3RDtBQUVsRCxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQTRDLGtDQUFZO0lBRHhEO1FBQUEscUVBeUNDO1FBdENHLFVBQUksR0FBWSxJQUFJLENBQUM7O0lBc0N6QixDQUFDO0lBckNHLDhCQUFLLEdBQUw7UUFDSSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRCxRQUFRLENBQUMsRUFBRSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCx1Q0FBYyxHQUFkLFVBQWUsS0FBSztRQUFwQixpQkFnQ0M7UUEvQkcsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO1lBQzVDLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxjQUFjLEVBQUU7Z0JBQ2hHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFZLENBQUMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5RCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzVELEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNyRCxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQWEsQ0FBQyxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQzFFLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO2dCQUM3RCxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDZCxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNYO1NBQ0o7YUFDSSxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsY0FBYyxFQUFFO1lBQ3pHLElBQUksR0FBRyxHQUFHLGlCQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksR0FBRyxJQUFJLENBQUMsRUFBRTtnQkFDVixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1RCxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDM0UsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3JELElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO2dCQUM3RCxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDZCxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBWSxDQUFDLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hELENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNaO1NBQ0o7SUFDTCxDQUFDO0lBckNEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ0c7SUFGSixjQUFjO1FBRGxDLE9BQU87T0FDYSxjQUFjLENBd0NsQztJQUFELHFCQUFDO0NBeENELEFBd0NDLENBeEMyQyxFQUFFLENBQUMsU0FBUyxHQXdDdkQ7a0JBeENvQixjQUFjIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuaW1wb3J0IEFJQ29udHJvbGxlciBmcm9tIFwiLi9BSUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEJveENvbnRyb2xsZXIgZnJvbSBcIi4vQm94Q29udHJvbGxlclwiO1xyXG5pbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29sbGlkZXJBdHRhY2sgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBteUFJOiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIGxldCBjb2xsaWRlciA9IHRoaXMuZ2V0Q29tcG9uZW50KGNjLkNvbGxpZGVyM0QpO1xyXG4gICAgICAgIGNvbGxpZGVyLm9uKCd0cmlnZ2VyLWVudGVyJywgdGhpcy5vblRyaWdnZXJFbnRlciwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBvblRyaWdnZXJFbnRlcihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ3JvdXAgPT0gXCJNb2RlbDJcIikge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLm5hbWUgPT0gXCJCb3hcIiAmJiAhdGhpcy5teUFJLmdldENvbXBvbmVudChBSUNvbnRyb2xsZXIpLmJvb2xDaGVja0RlYXRoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5teUFJLnN0b3BBbGxBY3Rpb25zKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXR0YWNrXCIpO1xyXG4gICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRBdHRhY2ssIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5nZXRDb21wb25lbnQoQm94Q29udHJvbGxlcikuRGllV2hlbkVuZW15QXR0YWNrKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teUFJLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIklkbGVcIilcclxuICAgICAgICAgICAgICAgIH0sIDAuNzA4KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAxLjMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKGV2ZW50Lm90aGVyQ29sbGlkZXIubm9kZS5ncm91cCA9PSBcIlBsYXllclwiICYmICF0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuYm9vbENoZWNrRGVhdGgpIHtcclxuICAgICAgICAgICAgbGV0IFJhbiA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VJbnRlZ2VyKDEsIDUpO1xyXG4gICAgICAgICAgICBpZiAoUmFuID09IDIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubXlBSS5nZXRDb21wb25lbnQoQUlDb250cm9sbGVyKS51bnNjaGVkdWxlQWxsQ2FsbGJhY2tzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm15QUkuc3RvcEFsbEFjdGlvbnMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubXlBSS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBdHRhY2tcIik7XHJcbiAgICAgICAgICAgICAgICBldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkNoYXJhY3RlckZhbGwoKTtcclxuICAgICAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teUFJLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIklkbGVcIilcclxuICAgICAgICAgICAgICAgIH0sIDAuNzA4KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm15QUkuZ2V0Q29tcG9uZW50KEFJQ29udHJvbGxlcikuRW5lbXlSdW5JZGxlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAwLjg1KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=