
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Controller/CharacterController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '56058bsB5JCBYLEHDj4omnN', 'CharacterController');
// Scripts/Controller/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrickData_1 = require("../Brick/BrickData");
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Utility_1 = require("../Common/Utility");
var CheckMoveDownStairs_1 = require("./CheckMoveDownStairs");
var JoystickFollow_1 = require("./JoystickFollow");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        // @property(cc.Node)
        // ArrowDirection: cc.Node = null;
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.speedMove = 20;
        _this.timeAnim = 0;
        // @property(cc.Node)
        // placeVfxWeapon: cc.Node = null;
        _this.brickHom = null;
        _this.brickStick = null;
        _this.checkMoveDownStairs = null;
        _this.brickType = EnumDefine_1.BrickType.BrickCharacter;
        _this.level = 0;
        _this.x = 0.003;
        _this.lastPositionStickY = 0.013;
        _this.z = -0.002;
        _this.clampTopY = 0;
        _this.listBrickAdd = [];
        _this.boolCanAttack = true;
        _this.boolMoveInFloor = false;
        _this.boolPlaySoundFoot = false;
        _this.boolCheckTypeRotate = false;
        _this.rigidbody = null;
        return _this;
    }
    CharacterController.prototype.onEnable = function () {
        GamePlayInstance_1.eventDispatcher.on(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.onDisable = function () {
        GamePlayInstance_1.eventDispatcher.off(KeyEvent_1.default.increaseStickMyCharacter, this.IncreaseStick, this);
    };
    CharacterController.prototype.start = function () {
        this.rigidbody = this.node.getComponent(cc.RigidBody3D);
        var collider = this.getComponent(cc.Collider3D);
        collider.on('collision-stay', this.onCollisionStay, this);
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
    };
    CharacterController.prototype.onCollisionStay = function (event) {
        if (event.otherCollider.node.group == "Floor") {
            this.boolMoveInFloor = true;
            this.boolCanAttack = true;
        }
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "StairsBoxCollider") {
                this.boolCanAttack = false;
            }
        }
    };
    CharacterController.prototype.onCollisionExit = function (event) {
        this.boolMoveInFloor = false;
    };
    CharacterController.prototype.update = function (dt) {
        if (Global_1.default.boolEnableTouch) {
            if (!this.checkMoveDownStairs.getComponent(CheckMoveDownStairs_1.default).boolMoveDown && this.boolMoveInFloor)
                this.MovePhyics(0);
            else
                this.MovePhyics(-20);
        }
        this.ClampTopYByLevel();
    };
    CharacterController.prototype.Attacking = function () {
        if (!Global_1.default.boolStartAttacking) {
            if (this.boolCanAttack) {
                // Global.boolStartAttacking = true;
                // Global.boolCheckAttacking = false;
                // this.node.getComponent(cc.SkeletonAnimation).play("Attack");
                // cc.audioEngine.playEffect(Global.soundAttack, false);
                // this.scheduleOnce(() => {
                //     Global.boolCheckAttacking = true;
                //     Global.boolCheckAttacked = true;
                // }, 0.18);
                // this.scheduleOnce(() => {
                Global_1.default.boolStartAttacking = false;
                Global_1.default.boolFirstTouchJoyStick = false;
                this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
                // }, this.timeAnim);
            }
            else {
                this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
                Global_1.default.boolFirstTouchJoyStick = false;
            }
        }
    };
    CharacterController.prototype.MovePhyics = function (z) {
        var _this = this;
        this.node.y = cc.misc.clampf(this.node.y, -33, this.clampTopY);
        if (!this.boolPlaySoundFoot) {
            this.boolPlaySoundFoot = true;
            cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
            this.scheduleOnce(function () {
                _this.boolPlaySoundFoot = false;
            }, 0.3);
        }
        var degree = Utility_1.default.CaculatorDegree(Global_1.default.touchPos);
        this.node.is3DNode = true;
        if (!this.boolCheckTypeRotate) {
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
        else {
            this.node.runAction(cc.sequence(cc.rotate3DTo(0.1, cc.v3(-90, 180, degree)), cc.callFunc(function () {
                _this.boolCheckTypeRotate = false;
            })));
        }
        this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Global_1.default.touchPos.x, this.speedMove * Global_1.default.touchPos.y, z));
    };
    CharacterController.prototype.StopMove = function () {
        this.rigidbody.setLinearVelocity(new cc.Vec3(0, 0, 0));
    };
    CharacterController.prototype.SpawnerEffectSmoke = function (smoke) {
        // let Smoke = cc.instantiate(smoke);
        // Smoke.parent = cc.Canvas.instance.node;
        // // let pos = this.node.convertToWorldSpaceAR(this.placeVfxWeapon.getPosition());
        // pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        // Smoke.x = pos.x;
        // Smoke.y = pos.y;
        // Smoke.z = 0;
    };
    CharacterController.prototype.IncreaseStick = function () {
        var brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData_1.default).setmaterial(EnumDefine_1.BrickType.BrickCharacter);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    };
    CharacterController.prototype.DecreaseStick = function () {
        this.listBrickAdd[this.listBrickAdd.length - 1].destroy();
        this.lastPositionStickY = this.lastPositionStickY - 0.004;
        this.listBrickAdd.pop();
    };
    CharacterController.prototype.IncreaseLevel = function () {
        this.node.scale += 4;
        this.level++;
        if (this.level >= 5)
            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.txtClimb.runAction(cc.fadeIn(0.3));
    };
    CharacterController.prototype.DecreaseLevel = function () {
        this.node.scale -= 5;
        this.level--;
    };
    CharacterController.prototype.SpawnerBrickWhenFall = function () {
        if (this.level > 0) {
            for (var i = 0; i < this.listBrickAdd.length; i++) {
                var x = Utility_1.default.RandomRangeFloat(-7, 7);
                var y = Utility_1.default.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y);
            }
            this.DecreaseStickAll();
        }
    };
    CharacterController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    };
    CharacterController.prototype.DecreaseStickAll = function () {
        for (var i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.level = 0;
    };
    CharacterController.prototype.ClampTopYByLevel = function () {
        switch (this.level) {
            case 0:
                this.clampTopY = 32;
                break;
            case 1:
                this.clampTopY = 36;
                break;
            case 2:
                this.clampTopY = 40;
                break;
            case 3:
                this.clampTopY = 44;
                break;
            case 4:
                this.clampTopY = 48;
                break;
            case 5:
                this.clampTopY = 52;
                break;
            case 6:
                this.clampTopY = 56;
                break;
            case 7:
                this.clampTopY = 60;
                break;
            case 8:
                this.clampTopY = 64;
                break;
            default:
                this.clampTopY = 200;
        }
    };
    CharacterController.prototype.CharacterFall = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.opacity = 0;
        this.SpawnerBrickWhenFall();
        Global_1.default.boolEnableTouch = false;
        this.boolCheckTypeRotate = true;
        Global_1.default.boolCharacterFall = true;
        GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyDot.setPosition(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.joyStick.getComponent(JoystickFollow_1.default).joyRing.getPosition());
        this.StopMove();
        this.scheduleOnce(function () {
            Global_1.default.boolCharacterFall = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
            Global_1.default.boolFirstTouchJoyStick = false;
        }, 1.28);
    };
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speedMove", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickHom", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "brickStick", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "checkMoveDownStairs", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.BrickType) })
    ], CharacterController.prototype, "brickType", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29udHJvbGxlclxcQ2hhcmFjdGVyQ29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxnREFBMkM7QUFDM0MsbURBQWlEO0FBQ2pELCtEQUErRTtBQUMvRSwyQ0FBc0M7QUFDdEMsK0NBQTBDO0FBQzFDLDZDQUF3QztBQUN4Qyw2REFBd0Q7QUFDeEQsbURBQTZDO0FBRXZDLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBaUQsdUNBQVk7SUFEN0Q7UUFHSSxxQkFBcUI7UUFDckIsa0NBQWtDO1FBSnRDLHFFQStOQztRQXhORyxlQUFTLEdBQVcsRUFBRSxDQUFDO1FBR3ZCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFFckIscUJBQXFCO1FBQ3JCLGtDQUFrQztRQUdsQyxjQUFRLEdBQWMsSUFBSSxDQUFDO1FBRzNCLGdCQUFVLEdBQWMsSUFBSSxDQUFDO1FBRzdCLHlCQUFtQixHQUFZLElBQUksQ0FBQztRQUc3QixlQUFTLEdBQWMsc0JBQVMsQ0FBQyxjQUFjLENBQUM7UUFFdkQsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixPQUFDLEdBQVcsS0FBSyxDQUFDO1FBQ2xCLHdCQUFrQixHQUFXLEtBQUssQ0FBQztRQUNuQyxPQUFDLEdBQVcsQ0FBQyxLQUFLLENBQUM7UUFDbkIsZUFBUyxHQUFXLENBQUMsQ0FBQztRQUN0QixrQkFBWSxHQUFjLEVBQUUsQ0FBQztRQUM3QixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUM5QixxQkFBZSxHQUFZLEtBQUssQ0FBQztRQUNqQyx1QkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMseUJBQW1CLEdBQVksS0FBSyxDQUFDO1FBQzlCLGVBQVMsR0FBbUIsSUFBSSxDQUFDOztJQTBMNUMsQ0FBQztJQXhMRyxzQ0FBUSxHQUFSO1FBQ0ksa0NBQWUsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3BGLENBQUM7SUFDRCx1Q0FBUyxHQUFUO1FBQ0ksa0NBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFDRCxtQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDeEQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDaEQsUUFBUSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFDRCw2Q0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDakIsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxFQUFFO1lBQzNDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQzVCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksT0FBTyxFQUFFO1lBQzNDLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLG1CQUFtQixFQUFFO2dCQUN0RCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQzthQUM5QjtTQUNKO0lBQ0wsQ0FBQztJQUNELDZDQUFlLEdBQWYsVUFBZ0IsS0FBSztRQUNqQixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUNqQyxDQUFDO0lBQ0Qsb0NBQU0sR0FBTixVQUFPLEVBQUU7UUFDTCxJQUFJLGdCQUFNLENBQUMsZUFBZSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLDZCQUFtQixDQUFDLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxlQUFlO2dCQUNoRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDOztnQkFFbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzVCO1FBQ0QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNELHVDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3BCLG9DQUFvQztnQkFDcEMscUNBQXFDO2dCQUNyQywrREFBK0Q7Z0JBQy9ELHdEQUF3RDtnQkFDeEQsNEJBQTRCO2dCQUM1Qix3Q0FBd0M7Z0JBQ3hDLHVDQUF1QztnQkFDdkMsWUFBWTtnQkFDWiw0QkFBNEI7Z0JBQ3hCLGdCQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2dCQUNsQyxnQkFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztnQkFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN2RSxxQkFBcUI7YUFDeEI7aUJBQ0k7Z0JBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUNuRSxnQkFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQzthQUN6QztTQUNKO0lBQ0wsQ0FBQztJQUNELHdDQUFVLEdBQVYsVUFBVyxDQUFTO1FBQXBCLGlCQW9CQztRQW5CRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN6QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUNuQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDWDtRQUNELElBQUksTUFBTSxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLGdCQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUN6RDthQUNJO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ3JGLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdILENBQUM7SUFDRCxzQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFDRCxnREFBa0IsR0FBbEIsVUFBbUIsS0FBZ0I7UUFDL0IscUNBQXFDO1FBQ3JDLDBDQUEwQztRQUMxQyxtRkFBbUY7UUFDbkYsMkRBQTJEO1FBQzNELG1CQUFtQjtRQUNuQixtQkFBbUI7UUFDbkIsZUFBZTtJQUNuQixDQUFDO0lBQ0QsMkNBQWEsR0FBYjtRQUNJLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2pCLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUMxQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDakIsS0FBSyxDQUFDLFlBQVksQ0FBQyxtQkFBUyxDQUFDLENBQUMsV0FBVyxDQUFDLHNCQUFTLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzFELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUMxRCxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDO1lBQ2YsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNqQixDQUFDO0lBQ0Qsa0RBQW9CLEdBQXBCO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRTtZQUNoQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxHQUFHLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxHQUFHLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO2FBQzFCO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDM0I7SUFDTCxDQUFDO0lBQ0QsMENBQVksR0FBWixVQUFhLENBQVMsRUFBRSxDQUFTO1FBQzdCLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQ3ZDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDdEIsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNaLElBQUksQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQzthQUMzRixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUNELDhDQUFnQixHQUFoQjtRQUNJLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xDO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO0lBQ25CLENBQUM7SUFDRCw4Q0FBZ0IsR0FBaEI7UUFDSSxRQUFRLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDaEIsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1YsS0FBSyxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixNQUFNO1lBQ1Y7Z0JBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7U0FDNUI7SUFDTCxDQUFDO0lBQ0QsMkNBQWEsR0FBYjtRQUFBLGlCQWNDO1FBYkcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFELDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUM1QixnQkFBTSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUNoQyxnQkFBTSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUNoQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyx3QkFBYSxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyx3QkFBYSxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDOU4sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxnQkFBTSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUNqQyxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDMUQsZ0JBQU0sQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDMUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQXZORDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzBEQUNFO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7eURBQ0E7SUFNckI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzt5REFDTztJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzJEQUNTO0lBRzdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7b0VBQ2tCO0lBR3BDO1FBREMsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsc0JBQVMsQ0FBQyxFQUFFLENBQUM7MERBQ2dCO0lBeEJ0QyxtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQThOdkM7SUFBRCwwQkFBQztDQTlORCxBQThOQyxDQTlOZ0QsRUFBRSxDQUFDLFNBQVMsR0E4TjVEO2tCQTlOb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEJyaWNrRGF0YSBmcm9tIFwiLi4vQnJpY2svQnJpY2tEYXRhXCI7XHJcbmltcG9ydCB7IEJyaWNrVHlwZSB9IGZyb20gXCIuLi9Db21tb24vRW51bURlZmluZVwiO1xyXG5pbXBvcnQgR2FtZVBsYXlJbnN0YW5jZSwgeyBldmVudERpc3BhdGNoZXIgfSBmcm9tIFwiLi4vQ29tbW9uL0dhbWVQbGF5SW5zdGFuY2VcIjtcclxuaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi4vQ29tbW9uL0dsb2JhbFwiO1xyXG5pbXBvcnQgS2V5RXZlbnQgZnJvbSBcIi4uL0NvbW1vbi9LZXlFdmVudFwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuaW1wb3J0IENoZWNrTW92ZURvd25TdGFpcnMgZnJvbSBcIi4vQ2hlY2tNb3ZlRG93blN0YWlyc1wiO1xyXG5pbXBvcnQgSm95c3RpY0ZvbGxvdyBmcm9tIFwiLi9Kb3lzdGlja0ZvbGxvd1wiO1xyXG5cclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENoYXJhY3RlckNvbnRyb2xsZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIC8vIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgLy8gQXJyb3dEaXJlY3Rpb246IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgc3BlZWRNb3ZlOiBudW1iZXIgPSAyMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHRpbWVBbmltOiBudW1iZXIgPSAwO1xyXG4gICAgXHJcbiAgICAvLyBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIC8vIHBsYWNlVmZ4V2VhcG9uOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgYnJpY2tIb206IGNjLlByZWZhYiA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcclxuICAgIGJyaWNrU3RpY2s6IGNjLlByZWZhYiA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBjaGVja01vdmVEb3duU3RhaXJzOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoeyB0eXBlOiBjYy5FbnVtKEJyaWNrVHlwZSkgfSlcclxuICAgIHB1YmxpYyBicmlja1R5cGU6IEJyaWNrVHlwZSA9IEJyaWNrVHlwZS5Ccmlja0NoYXJhY3RlcjtcclxuXHJcbiAgICBsZXZlbDogbnVtYmVyID0gMDtcclxuICAgIHg6IG51bWJlciA9IDAuMDAzO1xyXG4gICAgbGFzdFBvc2l0aW9uU3RpY2tZOiBudW1iZXIgPSAwLjAxMztcclxuICAgIHo6IG51bWJlciA9IC0wLjAwMjtcclxuICAgIGNsYW1wVG9wWTogbnVtYmVyID0gMDtcclxuICAgIGxpc3RCcmlja0FkZDogY2MuTm9kZVtdID0gW107XHJcbiAgICBib29sQ2FuQXR0YWNrOiBib29sZWFuID0gdHJ1ZTtcclxuICAgIGJvb2xNb3ZlSW5GbG9vcjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbFBsYXlTb3VuZEZvb3Q6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xDaGVja1R5cGVSb3RhdGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHB1YmxpYyByaWdpZGJvZHk6IGNjLlJpZ2lkQm9keTNEID0gbnVsbDtcclxuXHJcbiAgICBvbkVuYWJsZSgpIHtcclxuICAgICAgICBldmVudERpc3BhdGNoZXIub24oS2V5RXZlbnQuaW5jcmVhc2VTdGlja015Q2hhcmFjdGVyLCB0aGlzLkluY3JlYXNlU3RpY2ssIHRoaXMpO1xyXG4gICAgfVxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGV2ZW50RGlzcGF0Y2hlci5vZmYoS2V5RXZlbnQuaW5jcmVhc2VTdGlja015Q2hhcmFjdGVyLCB0aGlzLkluY3JlYXNlU3RpY2ssIHRoaXMpO1xyXG4gICAgfVxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5yaWdpZGJvZHkgPSB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlJpZ2lkQm9keTNEKTtcclxuICAgICAgICBsZXQgY29sbGlkZXIgPSB0aGlzLmdldENvbXBvbmVudChjYy5Db2xsaWRlcjNEKTtcclxuICAgICAgICBjb2xsaWRlci5vbignY29sbGlzaW9uLXN0YXknLCB0aGlzLm9uQ29sbGlzaW9uU3RheSwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5saXN0QnJpY2tBZGQuc3BsaWNlKDAsIHRoaXMubGlzdEJyaWNrQWRkLmxlbmd0aCk7XHJcbiAgICB9XHJcbiAgICBvbkNvbGxpc2lvblN0YXkoZXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQub3RoZXJDb2xsaWRlci5ub2RlLmdyb3VwID09IFwiRmxvb3JcIikge1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xNb3ZlSW5GbG9vciA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENhbkF0dGFjayA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUuZ3JvdXAgPT0gXCJNb2RlbFwiKSB7XHJcbiAgICAgICAgICAgIGlmIChldmVudC5vdGhlckNvbGxpZGVyLm5vZGUubmFtZSA9PSBcIlN0YWlyc0JveENvbGxpZGVyXCIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYm9vbENhbkF0dGFjayA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgb25Db2xsaXNpb25FeGl0KGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5ib29sTW92ZUluRmxvb3IgPSBmYWxzZTtcclxuICAgIH1cclxuICAgIHVwZGF0ZShkdCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbEVuYWJsZVRvdWNoKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5jaGVja01vdmVEb3duU3RhaXJzLmdldENvbXBvbmVudChDaGVja01vdmVEb3duU3RhaXJzKS5ib29sTW92ZURvd24gJiYgdGhpcy5ib29sTW92ZUluRmxvb3IpXHJcbiAgICAgICAgICAgICAgICB0aGlzLk1vdmVQaHlpY3MoMCk7XHJcbiAgICAgICAgICAgIGVsc2VcclxuICAgICAgICAgICAgICAgIHRoaXMuTW92ZVBoeWljcygtMjApO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLkNsYW1wVG9wWUJ5TGV2ZWwoKTtcclxuICAgIH1cclxuICAgIEF0dGFja2luZygpIHtcclxuICAgICAgICBpZiAoIUdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuYm9vbENhbkF0dGFjaykge1xyXG4gICAgICAgICAgICAgICAgLy8gR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAvLyBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXR0YWNrXCIpO1xyXG4gICAgICAgICAgICAgICAgLy8gY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRBdHRhY2ssIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIC8vICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIC8vICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgLy8gfSwgMC4xOCk7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIEdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFtb25nX1VTX2lkbGVcIik7XHJcbiAgICAgICAgICAgICAgICAvLyB9LCB0aGlzLnRpbWVBbmltKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBbW9uZ19VU19pZGxlXCIpO1xyXG4gICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xGaXJzdFRvdWNoSm95U3RpY2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIE1vdmVQaHlpY3MoejogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueSwgLTMzLCB0aGlzLmNsYW1wVG9wWSk7XHJcbiAgICAgICAgaWYgKCF0aGlzLmJvb2xQbGF5U291bmRGb290KSB7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbFBsYXlTb3VuZEZvb3QgPSB0cnVlO1xyXG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZEZvb3RTdGVwLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYm9vbFBsYXlTb3VuZEZvb3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSwgMC4zKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IGRlZ3JlZSA9IFV0aWxpdHkuQ2FjdWxhdG9yRGVncmVlKEdsb2JhbC50b3VjaFBvcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLmlzM0ROb2RlID0gdHJ1ZTtcclxuICAgICAgICBpZiAoIXRoaXMuYm9vbENoZWNrVHlwZVJvdGF0ZSkge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMygtOTAsIDE4MCwgZGVncmVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Mucm90YXRlM0RUbygwLjEsIGNjLnYzKC05MCwgMTgwLCBkZWdyZWUpKSwgY2MuY2FsbEZ1bmMoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tUeXBlUm90YXRlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0pKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucmlnaWRib2R5LnNldExpbmVhclZlbG9jaXR5KG5ldyBjYy5WZWMzKHRoaXMuc3BlZWRNb3ZlICogR2xvYmFsLnRvdWNoUG9zLngsIHRoaXMuc3BlZWRNb3ZlICogR2xvYmFsLnRvdWNoUG9zLnksIHopKTtcclxuICAgIH1cclxuICAgIFN0b3BNb3ZlKCkge1xyXG4gICAgICAgIHRoaXMucmlnaWRib2R5LnNldExpbmVhclZlbG9jaXR5KG5ldyBjYy5WZWMzKDAsIDAsIDApKTtcclxuICAgIH1cclxuICAgIFNwYXduZXJFZmZlY3RTbW9rZShzbW9rZTogY2MuUHJlZmFiKSB7XHJcbiAgICAgICAgLy8gbGV0IFNtb2tlID0gY2MuaW5zdGFudGlhdGUoc21va2UpO1xyXG4gICAgICAgIC8vIFNtb2tlLnBhcmVudCA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlO1xyXG4gICAgICAgIC8vIC8vIGxldCBwb3MgPSB0aGlzLm5vZGUuY29udmVydFRvV29ybGRTcGFjZUFSKHRoaXMucGxhY2VWZnhXZWFwb24uZ2V0UG9zaXRpb24oKSk7XHJcbiAgICAgICAgLy8gcG9zID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIocG9zKTtcclxuICAgICAgICAvLyBTbW9rZS54ID0gcG9zLng7XHJcbiAgICAgICAgLy8gU21va2UueSA9IHBvcy55O1xyXG4gICAgICAgIC8vIFNtb2tlLnogPSAwO1xyXG4gICAgfVxyXG4gICAgSW5jcmVhc2VTdGljaygpIHtcclxuICAgICAgICBsZXQgYnJpY2sgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLmJyaWNrU3RpY2spO1xyXG4gICAgICAgIGJyaWNrLnBhcmVudCA9IHRoaXMubm9kZS5jaGlsZHJlblsyXTtcclxuICAgICAgICBicmljay54ID0gdGhpcy54O1xyXG4gICAgICAgIGJyaWNrLnkgPSB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSArIDAuMDA0O1xyXG4gICAgICAgIGJyaWNrLnogPSB0aGlzLno7XHJcbiAgICAgICAgYnJpY2suZ2V0Q29tcG9uZW50KEJyaWNrRGF0YSkuc2V0bWF0ZXJpYWwoQnJpY2tUeXBlLkJyaWNrQ2hhcmFjdGVyKTtcclxuICAgICAgICB0aGlzLkluY3JlYXNlTGV2ZWwoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IHRoaXMubGFzdFBvc2l0aW9uU3RpY2tZICsgMC4wMDQ7XHJcbiAgICAgICAgdGhpcy5saXN0QnJpY2tBZGQucHVzaChicmljayk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZVN0aWNrKCkge1xyXG4gICAgICAgIHRoaXMubGlzdEJyaWNrQWRkW3RoaXMubGlzdEJyaWNrQWRkLmxlbmd0aCAtIDFdLmRlc3Ryb3koKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IHRoaXMubGFzdFBvc2l0aW9uU3RpY2tZIC0gMC4wMDQ7XHJcbiAgICAgICAgdGhpcy5saXN0QnJpY2tBZGQucG9wKCk7XHJcbiAgICB9XHJcbiAgICBJbmNyZWFzZUxldmVsKCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5zY2FsZSArPSA0O1xyXG4gICAgICAgIHRoaXMubGV2ZWwrKztcclxuICAgICAgICBpZiAodGhpcy5sZXZlbCA+PSA1KVxyXG4gICAgICAgICAgICBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LnR4dENsaW1iLnJ1bkFjdGlvbihjYy5mYWRlSW4oMC4zKSk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZUxldmVsKCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5zY2FsZSAtPSA1O1xyXG4gICAgICAgIHRoaXMubGV2ZWwtLTtcclxuICAgIH1cclxuICAgIFNwYXduZXJCcmlja1doZW5GYWxsKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmxldmVsID4gMCkge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubGlzdEJyaWNrQWRkLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgeCA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCgtNywgNyk7XHJcbiAgICAgICAgICAgICAgICBsZXQgeSA9IFV0aWxpdHkuUmFuZG9tUmFuZ2VGbG9hdCgtNywgNyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlNwYXduZXJCcmljayh4LCB5KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuRGVjcmVhc2VTdGlja0FsbCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFNwYXduZXJCcmljayh4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIGxldCBCcmljayA9IGNjLmluc3RhbnRpYXRlKHRoaXMuYnJpY2tIb20pO1xyXG4gICAgICAgIEJyaWNrLnBhcmVudCA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlO1xyXG4gICAgICAgIEJyaWNrLnggPSB0aGlzLm5vZGUueDtcclxuICAgICAgICBCcmljay55ID0gdGhpcy5ub2RlLnk7XHJcbiAgICAgICAgQnJpY2sueiA9IDE7XHJcbiAgICAgICAgbGV0IHQgPSBuZXcgY2MuVHdlZW4oKS50bygwLjMsIHsgcG9zaXRpb246IG5ldyBjYy5WZWMzKHRoaXMubm9kZS54ICsgeCwgdGhpcy5ub2RlLnkgKyB5LCAwLjUpIH0pXHJcbiAgICAgICAgICAgIC50YXJnZXQoQnJpY2spLnN0YXJ0KCk7XHJcbiAgICB9XHJcbiAgICBEZWNyZWFzZVN0aWNrQWxsKCkge1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5saXN0QnJpY2tBZGRbaV0uZGVzdHJveSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxpc3RCcmlja0FkZC5zcGxpY2UoMCwgdGhpcy5saXN0QnJpY2tBZGQubGVuZ3RoKTtcclxuICAgICAgICB0aGlzLmxhc3RQb3NpdGlvblN0aWNrWSA9IDAuMDEzO1xyXG4gICAgICAgIHRoaXMubm9kZS5jaGlsZHJlblswXS5zY2FsZSA9IDE7XHJcbiAgICAgICAgdGhpcy5sZXZlbCA9IDA7XHJcbiAgICB9XHJcbiAgICBDbGFtcFRvcFlCeUxldmVsKCkge1xyXG4gICAgICAgIHN3aXRjaCAodGhpcy5sZXZlbCkge1xyXG4gICAgICAgICAgICBjYXNlIDA6IHRoaXMuY2xhbXBUb3BZID0gMzI7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAxOiB0aGlzLmNsYW1wVG9wWSA9IDM2O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgMjogdGhpcy5jbGFtcFRvcFkgPSA0MDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIDM6IHRoaXMuY2xhbXBUb3BZID0gNDQ7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSA0OiB0aGlzLmNsYW1wVG9wWSA9IDQ4O1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgNTogdGhpcy5jbGFtcFRvcFkgPSA1MjtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIDY6IHRoaXMuY2xhbXBUb3BZID0gNTY7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSA3OiB0aGlzLmNsYW1wVG9wWSA9IDYwO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgODogdGhpcy5jbGFtcFRvcFkgPSA2NDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGFtcFRvcFkgPSAyMDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgQ2hhcmFjdGVyRmFsbCgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiRmFsbFwiKTtcclxuICAgICAgICBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LmpveVN0aWNrLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgIHRoaXMuU3Bhd25lckJyaWNrV2hlbkZhbGwoKTtcclxuICAgICAgICBHbG9iYWwuYm9vbEVuYWJsZVRvdWNoID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5ib29sQ2hlY2tUeXBlUm90YXRlID0gdHJ1ZTtcclxuICAgICAgICBHbG9iYWwuYm9vbENoYXJhY3RlckZhbGwgPSB0cnVlO1xyXG4gICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuam95U3RpY2suZ2V0Q29tcG9uZW50KEpveXN0aWNGb2xsb3cpLmpveURvdC5zZXRQb3NpdGlvbihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5LmpveVN0aWNrLmdldENvbXBvbmVudChKb3lzdGljRm9sbG93KS5qb3lSaW5nLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHRoaXMuU3RvcE1vdmUoKTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sQ2hhcmFjdGVyRmFsbCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiSWRsZVwiKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xGaXJzdFRvdWNoSm95U3RpY2sgPSBmYWxzZTtcclxuICAgICAgICB9LCAxLjI4KTtcclxuICAgIH1cclxufVxyXG4iXX0=