
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/GamePlayInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4c8358ev8RCn6trwZVq6/92', 'GamePlayInstance');
// Scripts/Common/GamePlayInstance.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BR1_1 = require("../GamePlay/BR1/BR1");
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
exports.eventDispatcher = new cc.EventTarget();
var GamePlayInstance = /** @class */ (function (_super) {
    __extends(GamePlayInstance, _super);
    function GamePlayInstance() {
        var _this = _super.call(this) || this;
        _this.gameplay = BR1_1.default.Instance(BR1_1.default);
        GamePlayInstance_1._instance = _this;
        return _this;
    }
    GamePlayInstance_1 = GamePlayInstance;
    var GamePlayInstance_1;
    GamePlayInstance = GamePlayInstance_1 = __decorate([
        ccclass
    ], GamePlayInstance);
    return GamePlayInstance;
}(Singleton_1.default));
exports.default = GamePlayInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHYW1lUGxheUluc3RhbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDJDQUFzQztBQUN0Qyx5Q0FBb0M7QUFDOUIsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUMvQixRQUFBLGVBQWUsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUVwRDtJQUE4QyxvQ0FBMkI7SUFFckU7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFKRCxjQUFRLEdBQVEsYUFBRyxDQUFDLFFBQVEsQ0FBQyxhQUFHLENBQUMsQ0FBQztRQUc5QixrQkFBZ0IsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUN0QyxDQUFDO3lCQUxnQixnQkFBZ0I7O0lBQWhCLGdCQUFnQjtRQURwQyxPQUFPO09BQ2EsZ0JBQWdCLENBTXBDO0lBQUQsdUJBQUM7Q0FORCxBQU1DLENBTjZDLG1CQUFTLEdBTXREO2tCQU5vQixnQkFBZ0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQlIxIGZyb20gXCIuLi9HYW1lUGxheS9CUjEvQlIxXCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4vU2luZ2xldG9uXCI7XHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbmV4cG9ydCBjb25zdCBldmVudERpc3BhdGNoZXIgPSBuZXcgY2MuRXZlbnRUYXJnZXQoKTtcclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZVBsYXlJbnN0YW5jZSBleHRlbmRzIFNpbmdsZXRvbjxHYW1lUGxheUluc3RhbmNlPiB7XHJcbiAgICBnYW1lcGxheTogQlIxID0gQlIxLkluc3RhbmNlKEJSMSk7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIEdhbWVQbGF5SW5zdGFuY2UuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxufVxyXG4iXX0=