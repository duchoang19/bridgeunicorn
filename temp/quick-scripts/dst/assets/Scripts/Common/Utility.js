
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Utility.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5445fCIuP1HJLq2hkCm2zpL', 'Utility');
// Scripts/Common/Utility.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Utility = /** @class */ (function (_super) {
    __extends(Utility, _super);
    function Utility() {
        var _this = _super.call(this) || this;
        Utility_1._instance = _this;
        return _this;
    }
    Utility_1 = Utility;
    Utility.RandomRangeFloat = function (lower, upper) {
        return Math.random() * (upper - lower) + lower;
        //return Math.floor(Math.random() * (lower - lower)) + lower;
    };
    Utility.RandomRangeInteger = function (lower, upper) {
        return Math.round(Math.random() * (upper - lower) + lower);
    };
    Utility.Distance = function (vec1, vec2) {
        var Distance = Math.sqrt(Math.pow(vec1.x - vec2.x, 2) +
            Math.pow(vec1.y - vec2.y, 2));
        return Distance;
    };
    Utility.BetweenDegree = function (comVec, dirVec) {
        var angleDegree = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDegree - 90;
    };
    Utility.CaculatorDegree = function (Target) {
        var r = Math.atan2(Target.y, Target.x);
        var degree = r * 180 / (Math.PI);
        degree = 360 - degree + 90;
        return degree;
    };
    var Utility_1;
    Utility = Utility_1 = __decorate([
        ccclass
    ], Utility);
    return Utility;
}(Singleton_1.default));
exports.default = Utility;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxVdGlsaXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHlDQUFvQztBQUU5QixJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQXFDLDJCQUFrQjtJQUNuRDtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQURHLFNBQU8sQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUM3QixDQUFDO2dCQUpnQixPQUFPO0lBS2pCLHdCQUFnQixHQUF2QixVQUF3QixLQUFhLEVBQUUsS0FBYTtRQUNoRCxPQUFPLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDL0MsNkRBQTZEO0lBQ2pFLENBQUM7SUFDTSwwQkFBa0IsR0FBekIsVUFBMEIsS0FBYSxFQUFFLEtBQWE7UUFDbEQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBQ00sZ0JBQVEsR0FBZixVQUFnQixJQUFhLEVBQUUsSUFBYTtRQUN4QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFDTSxxQkFBYSxHQUFwQixVQUFxQixNQUFlLEVBQUUsTUFBZTtRQUNqRCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUN2RixPQUFPLFdBQVcsR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNNLHVCQUFlLEdBQXRCLFVBQXVCLE1BQWU7UUFDbEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxJQUFJLE1BQU0sR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUMzQixPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOztJQTFCZ0IsT0FBTztRQUQzQixPQUFPO09BQ2EsT0FBTyxDQTJCM0I7SUFBRCxjQUFDO0NBM0JELEFBMkJDLENBM0JvQyxtQkFBUyxHQTJCN0M7a0JBM0JvQixPQUFPIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi9TaW5nbGV0b25cIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVdGlsaXR5IGV4dGVuZHMgU2luZ2xldG9uPFV0aWxpdHk+IHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgVXRpbGl0eS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG4gICAgc3RhdGljIFJhbmRvbVJhbmdlRmxvYXQobG93ZXI6IG51bWJlciwgdXBwZXI6IG51bWJlcikge1xyXG4gICAgICAgIHJldHVybiBNYXRoLnJhbmRvbSgpICogKHVwcGVyIC0gbG93ZXIpICsgbG93ZXI7XHJcbiAgICAgICAgLy9yZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKGxvd2VyIC0gbG93ZXIpKSArIGxvd2VyO1xyXG4gICAgfVxyXG4gICAgc3RhdGljIFJhbmRvbVJhbmdlSW50ZWdlcihsb3dlcjogbnVtYmVyLCB1cHBlcjogbnVtYmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqICh1cHBlciAtIGxvd2VyKSArIGxvd2VyKTtcclxuICAgIH1cclxuICAgIHN0YXRpYyBEaXN0YW5jZSh2ZWMxOiBjYy5WZWMyLCB2ZWMyOiBjYy5WZWMyKSB7XHJcbiAgICAgICAgbGV0IERpc3RhbmNlID0gTWF0aC5zcXJ0KE1hdGgucG93KHZlYzEueCAtIHZlYzIueCwgMikgK1xyXG4gICAgICAgICAgICBNYXRoLnBvdyh2ZWMxLnkgLSB2ZWMyLnksIDIpKTtcclxuICAgICAgICByZXR1cm4gRGlzdGFuY2U7XHJcbiAgICB9XHJcbiAgICBzdGF0aWMgQmV0d2VlbkRlZ3JlZShjb21WZWM6IGNjLlZlYzIsIGRpclZlYzogY2MuVmVjMikge1xyXG4gICAgICAgIGxldCBhbmdsZURlZ3JlZSA9IE1hdGguYXRhbjIoZGlyVmVjLnkgLSBjb21WZWMueSwgZGlyVmVjLnggLSBjb21WZWMueCkgKiAxODAgLyBNYXRoLlBJO1xyXG4gICAgICAgIHJldHVybiBhbmdsZURlZ3JlZSAtIDkwOyBcclxuICAgIH1cclxuICAgIHN0YXRpYyBDYWN1bGF0b3JEZWdyZWUoVGFyZ2V0OiBjYy5WZWMyKSB7XHJcbiAgICAgICAgdmFyIHIgPSBNYXRoLmF0YW4yKFRhcmdldC55LCBUYXJnZXQueCk7XHJcbiAgICAgICAgdmFyIGRlZ3JlZSA9IHIgKiAxODAgLyAoTWF0aC5QSSk7XHJcbiAgICAgICAgZGVncmVlID0gMzYwIC0gZGVncmVlICsgOTA7XHJcbiAgICAgICAgcmV0dXJuIGRlZ3JlZTtcclxuICAgIH1cclxufVxyXG4iXX0=