
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/ScrollNew.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b1b8bobEddLp5s23+RJ6V0W', 'ScrollNew');
// Scripts/Common/ScrollNew.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScrollNew = /** @class */ (function (_super) {
    __extends(ScrollNew, _super);
    function ScrollNew() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.speed = 80;
        _this.resetY = -1138;
        _this.YStart = 1138;
        _this.Bg1 = null;
        _this.Bg2 = null;
        _this.Temp1 = 0;
        _this.Temp2 = 0;
        return _this;
    }
    ScrollNew.prototype.update = function (dt) {
        var y = this.node.y;
        y -= this.speed * dt;
        if (y <= this.resetY) {
            if (this.Bg2.y > this.Bg1.y) {
                this.Bg1.y = this.YStart;
                this.Bg2.y = 0;
            }
            else {
                this.Bg2.y = this.YStart;
                this.Bg1.y = 0;
            }
            y = 0;
        }
        this.node.y = y;
    };
    __decorate([
        property()
    ], ScrollNew.prototype, "speed", void 0);
    __decorate([
        property()
    ], ScrollNew.prototype, "resetY", void 0);
    __decorate([
        property()
    ], ScrollNew.prototype, "YStart", void 0);
    __decorate([
        property(cc.Node)
    ], ScrollNew.prototype, "Bg1", void 0);
    __decorate([
        property(cc.Node)
    ], ScrollNew.prototype, "Bg2", void 0);
    ScrollNew = __decorate([
        ccclass
    ], ScrollNew);
    return ScrollNew;
}(cc.Component));
exports.default = ScrollNew;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTY3JvbGxOZXcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUF1Qyw2QkFBWTtJQURuRDtRQUFBLHFFQThCQztRQTNCRyxXQUFLLEdBQUcsRUFBRSxDQUFDO1FBRVgsWUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDO1FBRWYsWUFBTSxHQUFHLElBQUksQ0FBQztRQUVkLFNBQUcsR0FBWSxJQUFJLENBQUM7UUFFcEIsU0FBRyxHQUFZLElBQUksQ0FBQztRQUNwQixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7O0lBaUJ0QixDQUFDO0lBaEJHLDBCQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDcEIsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDdkIsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDekIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2xCO2lCQUNJO2dCQUNELElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNsQjtZQUNELENBQUMsR0FBRyxDQUFDLENBQUM7U0FDTDtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBMUJEO1FBREMsUUFBUSxFQUFFOzRDQUNBO0lBRVg7UUFEQyxRQUFRLEVBQUU7NkNBQ0k7SUFFZjtRQURDLFFBQVEsRUFBRTs2Q0FDRztJQUVkO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MENBQ0U7SUFFcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDRTtJQVZILFNBQVM7UUFEN0IsT0FBTztPQUNhLFNBQVMsQ0E2QjdCO0lBQUQsZ0JBQUM7Q0E3QkQsQUE2QkMsQ0E3QnNDLEVBQUUsQ0FBQyxTQUFTLEdBNkJsRDtrQkE3Qm9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2Nyb2xsTmV3IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuICAgIEBwcm9wZXJ0eSgpXHJcbiAgICBzcGVlZCA9IDgwO1xyXG4gICAgQHByb3BlcnR5KClcclxuICAgIHJlc2V0WSA9IC0xMTM4O1xyXG4gICAgQHByb3BlcnR5KClcclxuICAgIFlTdGFydCA9IDExMzg7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEJnMTogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEJnMjogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBUZW1wMTogbnVtYmVyID0gMDtcclxuICAgIFRlbXAyOiBudW1iZXIgPSAwO1xyXG4gICAgdXBkYXRlKGR0KSB7XHJcbiAgICAgICAgdmFyIHkgPSB0aGlzLm5vZGUueTtcclxuICAgICAgICB5IC09IHRoaXMuc3BlZWQgKiBkdDtcclxuICAgICAgICAgaWYgKHkgPD0gdGhpcy5yZXNldFkpIHtcclxuICAgICAgICBpZiAodGhpcy5CZzIueSA+IHRoaXMuQmcxLnkpIHtcclxuICAgICAgICAgICAgdGhpcy5CZzEueSA9IHRoaXMuWVN0YXJ0O1xyXG4gICAgICAgICAgICB0aGlzLkJnMi55ID0gMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuQmcyLnkgPSB0aGlzLllTdGFydDtcclxuICAgICAgICAgICAgdGhpcy5CZzEueSA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHkgPSAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm5vZGUueSA9IHk7XHJcbiAgICB9XHJcbn0iXX0=