
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Global.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '46e6fxcur1NFZbnIGcspwN3', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolCharacterFall: false,
    boolEndGame: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundCollect: null,
    soundClickBtn: null,
    soundBoxBroken: null,
    soundUpStairs: null,
    soundWin: null
};
exports.default = Global;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHbG9iYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsSUFBSSxNQUFNLEdBQVc7SUFDakIsUUFBUSxFQUFFLElBQUk7SUFDZCxlQUFlLEVBQUUsS0FBSztJQUN0QixzQkFBc0IsRUFBRSxLQUFLO0lBQzdCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLGtCQUFrQixFQUFFLEtBQUs7SUFDekIsa0JBQWtCLEVBQUUsS0FBSztJQUN6QixpQkFBaUIsRUFBRSxLQUFLO0lBQ3hCLGlCQUFpQixFQUFFLEtBQUs7SUFDeEIsV0FBVyxFQUFFLEtBQUs7SUFDbEIsT0FBTyxFQUFFLElBQUk7SUFDYixVQUFVLEVBQUUsSUFBSTtJQUNoQixXQUFXLEVBQUUsSUFBSTtJQUNqQixhQUFhLEVBQUUsSUFBSTtJQUNuQixZQUFZLEVBQUUsSUFBSTtJQUNsQixhQUFhLEVBQUUsSUFBSTtJQUNuQixjQUFjLEVBQUUsSUFBSTtJQUNwQixhQUFhLEVBQUUsSUFBSTtJQUNuQixRQUFRLEVBQUUsSUFBSTtDQUNqQixDQUFDO0FBQ0Ysa0JBQWUsTUFBTSxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIEdsb2JhbCB7XHJcbiAgICB0b3VjaFBvczogY2MuVmVjMixcclxuICAgIGJvb2xFbmFibGVUb3VjaDogYm9vbGVhbixcclxuICAgIGJvb2xGaXJzdFRvdWNoSm95U3RpY2s6IGJvb2xlYW4sXHJcbiAgICBib29sU3RhcnRQbGF5OiBib29sZWFuLFxyXG4gICAgYm9vbFN0YXJ0QXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBib29sZWFuLFxyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGJvb2xlYW4sXHJcbiAgICBib29sQ2hhcmFjdGVyRmFsbDogYm9vbGVhbixcclxuICAgIGJvb2xFbmRHYW1lOiBib29sZWFuLFxyXG4gICAgc291bmRCRzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRJbnRybzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRBdHRhY2s6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kRm9vdFN0ZXA6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kQ29sbGVjdDogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRDbGlja0J0bjogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRCb3hCcm9rZW46IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kVXBTdGFpcnM6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgc291bmRXaW46IGNjLkF1ZGlvQ2xpcFxyXG59XHJcbmxldCBHbG9iYWw6IEdsb2JhbCA9IHtcclxuICAgIHRvdWNoUG9zOiBudWxsLFxyXG4gICAgYm9vbEVuYWJsZVRvdWNoOiBmYWxzZSxcclxuICAgIGJvb2xGaXJzdFRvdWNoSm95U3RpY2s6IGZhbHNlLFxyXG4gICAgYm9vbFN0YXJ0UGxheTogZmFsc2UsXHJcbiAgICBib29sU3RhcnRBdHRhY2tpbmc6IGZhbHNlLFxyXG4gICAgYm9vbENoZWNrQXR0YWNraW5nOiBmYWxzZSxcclxuICAgIGJvb2xDaGVja0F0dGFja2VkOiBmYWxzZSxcclxuICAgIGJvb2xDaGFyYWN0ZXJGYWxsOiBmYWxzZSxcclxuICAgIGJvb2xFbmRHYW1lOiBmYWxzZSxcclxuICAgIHNvdW5kQkc6IG51bGwsXHJcbiAgICBzb3VuZEludHJvOiBudWxsLFxyXG4gICAgc291bmRBdHRhY2s6IG51bGwsXHJcbiAgICBzb3VuZEZvb3RTdGVwOiBudWxsLFxyXG4gICAgc291bmRDb2xsZWN0OiBudWxsLFxyXG4gICAgc291bmRDbGlja0J0bjogbnVsbCxcclxuICAgIHNvdW5kQm94QnJva2VuOiBudWxsLFxyXG4gICAgc291bmRVcFN0YWlyczogbnVsbCxcclxuICAgIHNvdW5kV2luOiBudWxsXHJcbn07XHJcbmV4cG9ydCBkZWZhdWx0IEdsb2JhbDsiXX0=