
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/BR1/BR1.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9a459Pv04ZAzJrDsCZSGkoc', 'BR1');
// Scripts/GamePlay/BR1/BR1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("../../Common/Global");
var Singleton_1 = require("../../Common/Singleton");
var AIController_1 = require("../../Controller/AIController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BR1 = /** @class */ (function (_super) {
    __extends(BR1, _super);
    function BR1() {
        var _this = _super.call(this) || this;
        _this.Guide = null;
        _this.txtCollectBlock = null;
        _this.myCharacter = null;
        _this.AIParent = null;
        _this.BoxParent = null;
        _this.txtClimb = null;
        _this.joyStick = null;
        _this.effectCongra = null;
        _this.btnDownLoad = null;
        _this.endGame = null;
        _this.boolEndGameOne = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        _this.boolcheckInteraction = false;
        BR1_1._instance = _this;
        return _this;
    }
    BR1_1 = BR1;
    BR1.prototype.start = function () {
        Global_1.default.boolStartPlay = true;
    };
    BR1.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
            for (var i = 0; i < this.AIParent.childrenCount; i++) {
                this.AIParent.children[i].getComponent(AIController_1.default).StartMove();
            }
            this.btnDownLoad.active = true;
            this.boolcheckInteraction = true;
        }
        if (this.myCharacter.y >= 70 && !this.boolEndGameOne) {
            this.boolEndGameOne = true;
            Global_1.default.boolEndGame = true;
            Global_1.default.boolEnableTouch = false;
            for (var i = 0; i < this.myCharacter.children[2].childrenCount; i++) {
                if (this.myCharacter.children[2].children[i].name == "BrickStick") {
                    this.myCharacter.children[2].children[i].active = false;
                }
            }
            for (var i = 0; i < this.AIParent.childrenCount; i++) {
                this.AIParent.children[i].active = false;
            }
            this.myCharacter.stopAllActions();
            this.joyStick.active = false;
            this.ScaleCongra();
            this.myCharacter.getComponent(cc.RigidBody3D).setLinearVelocity(new cc.Vec3(0, 0, 0));
            // this.myCharacter.getComponent(CharacterController).ArrowDirection.active = false;
            this.myCharacter.getComponent(cc.SkeletonAnimation).play("Victory");
            cc.audioEngine.playEffect(Global_1.default.soundWin, false);
            this.scheduleOnce(function () {
                _this.EndGame();
            }, 1.2);
        }
    };
    BR1.prototype.ScaleCongra = function () {
        var _this = this;
        this.effectCongra.scale = 0;
        this.effectCongra.opacity = 255;
        this.effectCongra.runAction(cc.sequence(cc.scaleTo(0.3, 1.3).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.effectCongra.runAction(cc.fadeOut(0.3));
            }, 0.7);
        })));
    };
    BR1.prototype.EndGame = function () {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(function () {
            cc.audioEngine.stopAllEffects();
        }, 2);
        this.endGame.active = true;
    };
    var BR1_1;
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "txtCollectBlock", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "myCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "AIParent", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "BoxParent", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "txtClimb", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "joyStick", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "effectCongra", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "btnDownLoad", void 0);
    __decorate([
        property(cc.Node)
    ], BR1.prototype, "endGame", void 0);
    BR1 = BR1_1 = __decorate([
        ccclass
    ], BR1);
    return BR1;
}(Singleton_1.default));
exports.default = BR1;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXEJSMVxcQlIxLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDhDQUF5QztBQUN6QyxvREFBK0M7QUFDL0MsOERBQXlEO0FBR25ELElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBaUMsdUJBQWM7SUEwQjNDO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBM0JELFdBQUssR0FBWSxJQUFJLENBQUM7UUFFdEIscUJBQWUsR0FBWSxJQUFJLENBQUM7UUFFaEMsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFFNUIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUV6QixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRTFCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFFekIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUV6QixrQkFBWSxHQUFZLElBQUksQ0FBQztRQUU3QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUU1QixhQUFPLEdBQVksSUFBSSxDQUFDO1FBQ3hCLG9CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLGdCQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsWUFBTSxHQUFZLEtBQUssQ0FBQztRQUN4QiwwQkFBb0IsR0FBWSxLQUFLLENBQUM7UUFHbEMsS0FBRyxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUM7O0lBQ3pCLENBQUM7WUE3QmdCLEdBQUc7SUE4QnBCLG1CQUFLLEdBQUw7UUFDSSxnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUNELG9CQUFNLEdBQU47UUFBQSxpQkFtQ0M7UUFsQ0csSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUN0RCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BDO1lBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsc0JBQVksQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQ3BFO1lBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQy9CLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDM0IsZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQzFCLGdCQUFNLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUMvQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNqRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksWUFBWSxFQUFFO29CQUMvRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztpQkFDM0Q7YUFDSjtZQUNELEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFDbkQ7Z0JBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzthQUM1QztZQUNELElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDbEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0RixvRkFBb0Y7WUFDcEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3BFLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ25CLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUNYO0lBQ0wsQ0FBQztJQUNELHlCQUFXLEdBQVg7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUNqRyxLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNqRCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBQ0QscUJBQU8sR0FBUDtRQUNJLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxFQUFFLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUMvQixDQUFDOztJQTFGRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3NDQUNJO0lBRXRCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ2M7SUFFaEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDVTtJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3lDQUNPO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MENBQ1E7SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5Q0FDTztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3lDQUNPO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ1c7SUFFN0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDVTtJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3dDQUNNO0lBcEJQLEdBQUc7UUFEdkIsT0FBTztPQUNhLEdBQUcsQ0E2RnZCO0lBQUQsVUFBQztDQTdGRCxBQTZGQyxDQTdGZ0MsbUJBQVMsR0E2RnpDO2tCQTdGb0IsR0FBRyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBHbG9iYWwgZnJvbSBcIi4uLy4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgQUlDb250cm9sbGVyIGZyb20gXCIuLi8uLi9Db250cm9sbGVyL0FJQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vLi4vQ29udHJvbGxlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5kZWNsYXJlIGNvbnN0IHdpbmRvdzogYW55O1xyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCUjEgZXh0ZW5kcyBTaW5nbGV0b248QlIxPiB7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEd1aWRlOiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgdHh0Q29sbGVjdEJsb2NrOiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgbXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBBSVBhcmVudDogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEJveFBhcmVudDogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dENsaW1iOiBjYy5Ob2RlID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgam95U3RpY2s6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBlZmZlY3RDb25ncmE6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBidG5Eb3duTG9hZDogY2MuTm9kZSA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZEdhbWU6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgYm9vbEVuZEdhbWVPbmU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlyb25zb3VyY2U6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG1pbmR3b3JrczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdnVuZ2xlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBib29sY2hlY2tJbnRlcmFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBCUjEuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhdGhpcy5ib29sY2hlY2tJbnRlcmFjdGlvbikge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuaW50ZXJhY3Rpb24oKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuQUlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLkFJUGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChBSUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuYnRuRG93bkxvYWQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sY2hlY2tJbnRlcmFjdGlvbiA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLm15Q2hhcmFjdGVyLnkgPj0gNzAgJiYgIXRoaXMuYm9vbEVuZEdhbWVPbmUpIHtcclxuICAgICAgICAgICAgdGhpcy5ib29sRW5kR2FtZU9uZSA9IHRydWU7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5kR2FtZSA9IHRydWU7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm15Q2hhcmFjdGVyLmNoaWxkcmVuWzJdLmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMl0uY2hpbGRyZW5baV0ubmFtZSA9PSBcIkJyaWNrU3RpY2tcIikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubXlDaGFyYWN0ZXIuY2hpbGRyZW5bMl0uY2hpbGRyZW5baV0uYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuQUlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLkFJUGFyZW50LmNoaWxkcmVuW2ldLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMubXlDaGFyYWN0ZXIuc3RvcEFsbEFjdGlvbnMoKTtcclxuICAgICAgICAgICAgdGhpcy5qb3lTdGljay5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5TY2FsZUNvbmdyYSgpO1xyXG4gICAgICAgICAgICB0aGlzLm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChjYy5SaWdpZEJvZHkzRCkuc2V0TGluZWFyVmVsb2NpdHkobmV3IGNjLlZlYzMoMCwgMCwgMCkpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJWaWN0b3J5XCIpO1xyXG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KEdsb2JhbC5zb3VuZFdpbiwgZmFsc2UpO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLkVuZEdhbWUoKTtcclxuICAgICAgICAgICAgfSwgMS4yKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBTY2FsZUNvbmdyYSgpIHtcclxuICAgICAgICB0aGlzLmVmZmVjdENvbmdyYS5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5lZmZlY3RDb25ncmEub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLmVmZmVjdENvbmdyYS5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjMsIDEuMykuZWFzaW5nKGNjLmVhc2VCb3VuY2VPdXQoKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lZmZlY3RDb25ncmEucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4zKSk7XHJcbiAgICAgICAgICAgIH0sIDAuNyk7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuICAgIEVuZEdhbWUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMubWluZHdvcmtzKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5nYW1lRW5kICYmIHdpbmRvdy5nYW1lRW5kKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgd2luZG93Lk5VQy50cmlnZ2VyLmVuZEdhbWUoJ3dpbicpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnZ1bmdsZSkge1xyXG4gICAgICAgICAgICBwYXJlbnQucG9zdE1lc3NhZ2UoJ2NvbXBsZXRlJywgJyonKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5zdG9wQWxsRWZmZWN0cygpO1xyXG4gICAgICAgIH0sIDIpO1xyXG4gICAgICAgIHRoaXMuZW5kR2FtZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==