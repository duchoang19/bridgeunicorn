"use strict";
cc._RF.push(module, '532c3wlv65DxqpVK0UVc+yj', 'BrickHom');
// Scripts/Brick/BrickHom.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var AIController_1 = require("../Controller/AIController");
var CharacterController_1 = require("../Controller/CharacterController");
var BrickData_1 = require("./BrickData");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BrickHom = /** @class */ (function (_super) {
    __extends(BrickHom, _super);
    function BrickHom() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.x = 0.003;
        _this.z = -0.002;
        return _this;
    }
    BrickHom.prototype.start = function () {
        var _this = this;
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.BoxCollider3D).enabled = true;
        }, 1);
    };
    BrickHom.prototype.onTriggerEnter = function (event) {
        var _this = this;
        if (event.otherCollider.node.group == "Player") {
            var otherNode = event.otherCollider.node;
            cc.audioEngine.playEffect(Global_1.default.soundCollect, false);
            var pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            var pos = otherNode.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData_1.default).setmaterial(otherNode.getComponent(CharacterController_1.default).brickType);
            this.node.parent = otherNode.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode.getComponent(CharacterController_1.default).lastPositionStickY + 0.004, this.z) })
                .call(function () {
                GamePlayInstance_1.eventDispatcher.emit(KeyEvent_1.default.increaseStickMyCharacter);
                _this.node.destroy();
            }).target(this.node).start();
        }
        if (event.otherCollider.node.group == "Enemy") {
            var otherNode_1 = event.otherCollider.node;
            cc.audioEngine.playEffect(Global_1.default.soundCollect, false);
            var pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            var pos = otherNode_1.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData_1.default).setmaterial(otherNode_1.getComponent(AIController_1.default).brickType);
            this.node.parent = otherNode_1.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode_1.getComponent(AIController_1.default).lastPositionStickY + 0.004, this.z) })
                .call(function () {
                GamePlayInstance_1.eventDispatcher.emit(otherNode_1.getComponent(AIController_1.default).stringIncrease);
                _this.node.destroy();
            }).target(this.node).start();
        }
    };
    BrickHom = __decorate([
        ccclass
    ], BrickHom);
    return BrickHom;
}(cc.Component));
exports.default = BrickHom;

cc._RF.pop();