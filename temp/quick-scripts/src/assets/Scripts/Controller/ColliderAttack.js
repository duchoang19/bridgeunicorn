"use strict";
cc._RF.push(module, 'ede90aP+upFTbO0wWH/S0jG', 'ColliderAttack');
// Scripts/Controller/ColliderAttack.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var AIController_1 = require("./AIController");
var BoxController_1 = require("./BoxController");
var CharacterController_1 = require("./CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ColliderAttack = /** @class */ (function (_super) {
    __extends(ColliderAttack, _super);
    function ColliderAttack() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.myAI = null;
        return _this;
    }
    ColliderAttack.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    };
    ColliderAttack.prototype.onTriggerEnter = function (event) {
        var _this = this;
        if (event.otherCollider.node.group == "Model2") {
            if (event.otherCollider.node.name == "Box" && !this.myAI.getComponent(AIController_1.default).boolCheckDeath) {
                this.myAI.getComponent(AIController_1.default).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                event.otherCollider.node.getComponent(BoxController_1.default).DieWhenEnemyAttack();
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(cc.SkeletonAnimation).play("Idle");
                }, 0.708);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(AIController_1.default).EnemyRunIdle();
                }, 1.3);
            }
        }
        else if (event.otherCollider.node.group == "Player" && !this.myAI.getComponent(AIController_1.default).boolCheckDeath) {
            var Ran = Utility_1.default.RandomRangeInteger(1, 5);
            if (Ran == 2) {
                this.myAI.getComponent(AIController_1.default).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                event.otherCollider.node.getComponent(CharacterController_1.default).CharacterFall();
                cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(cc.SkeletonAnimation).play("Idle");
                }, 0.708);
                this.scheduleOnce(function () {
                    _this.myAI.getComponent(AIController_1.default).EnemyRunIdle();
                }, 0.85);
            }
        }
    };
    __decorate([
        property(cc.Node)
    ], ColliderAttack.prototype, "myAI", void 0);
    ColliderAttack = __decorate([
        ccclass
    ], ColliderAttack);
    return ColliderAttack;
}(cc.Component));
exports.default = ColliderAttack;

cc._RF.pop();