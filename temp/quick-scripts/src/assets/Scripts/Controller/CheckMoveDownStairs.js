"use strict";
cc._RF.push(module, '9b8d8xvinNMPYvAQa26/D07', 'CheckMoveDownStairs');
// Scripts/Controller/CheckMoveDownStairs.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CheckMoveDownStairs = /** @class */ (function (_super) {
    __extends(CheckMoveDownStairs, _super);
    function CheckMoveDownStairs() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.boolMoveDown = false;
        return _this;
    }
    CheckMoveDownStairs.prototype.start = function () {
        var collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-stay', this.onTriggerStay, this);
        collider.on('trigger-exit', this.onTriggerExit, this);
    };
    CheckMoveDownStairs.prototype.onTriggerStay = function (event) {
        if (event.otherCollider.node.group == "Model") {
            if (event.otherCollider.node.name == "CheckPlayerMoveBack") {
                this.boolMoveDown = true;
            }
        }
    };
    CheckMoveDownStairs.prototype.onTriggerExit = function (event) {
        this.boolMoveDown = false;
    };
    CheckMoveDownStairs = __decorate([
        ccclass
    ], CheckMoveDownStairs);
    return CheckMoveDownStairs;
}(cc.Component));
exports.default = CheckMoveDownStairs;

cc._RF.pop();