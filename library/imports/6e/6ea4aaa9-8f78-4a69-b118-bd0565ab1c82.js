"use strict";
cc._RF.push(module, '6ea4aqpj3hKabEYvQVlqxyC', 'BoxController');
// Scripts/Controller/BoxController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BoxController = /** @class */ (function (_super) {
    __extends(BoxController, _super);
    function BoxController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.brickHom = null;
        _this.timeBroken = 1.06;
        _this.boolCheckDeathOne = false;
        _this.boolCheckDeath = false;
        _this.boolCheckdelay = false;
        return _this;
    }
    BoxController.prototype.update = function () {
        var _this = this;
        if (!this.boolCheckDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y));
                var maxDistance = Utility_1.default.Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.BrokenBox();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                            //Global.boolCheckAttacked = false;
                            //Global.boolCheckAttacking = false;
                        }
                    }
                }
                if (!this.boolCheckdelay) {
                    this.boolCheckdelay = true;
                    this.scheduleOnce(function () {
                        Global_1.default.boolCheckAttacking = false;
                        _this.boolCheckdelay = false;
                    }, 0.003);
                }
            }
        }
    };
    BoxController.prototype.DieWhenEnemyAttack = function () {
        this.boolCheckDeath = true;
        this.BrokenBox();
    };
    BoxController.prototype.BrokenBox = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Broken");
        cc.audioEngine.playEffect(Global_1.default.soundBoxBroken, false);
        this.node.getComponent(cc.BoxCollider3D).enabled = false;
        this.scheduleOnce(function () {
            // for( let i = 0; i < 20; i++)
            // {
            //     this.SpawnerBrick(this.node.x - 2, this.node.y + 2);
            // }
            _this.SpawnerBrick(_this.node.x - 2, _this.node.y + 2);
            _this.SpawnerBrick(_this.node.x + 2, _this.node.y + 2);
            _this.SpawnerBrick(_this.node.x - 2, _this.node.y - 2);
            _this.SpawnerBrick(_this.node.x + 2, _this.node.y - 2);
        }, this.timeBroken / 7);
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.SkeletonAnimation).stop();
        }, this.timeBroken);
    };
    BoxController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(x, y, 0.5) })
            .call(function () {
            Brick.getComponent(cc.BoxCollider3D).enabled = true;
        })
            .target(Brick).start();
    };
    __decorate([
        property(cc.Prefab)
    ], BoxController.prototype, "brickHom", void 0);
    BoxController = __decorate([
        ccclass
    ], BoxController);
    return BoxController;
}(cc.Component));
exports.default = BoxController;

cc._RF.pop();