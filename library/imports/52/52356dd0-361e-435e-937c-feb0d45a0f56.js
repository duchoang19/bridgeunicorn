"use strict";
cc._RF.push(module, '523563QNh5DXpN8/rDUWg9W', 'AIController');
// Scripts/Controller/AIController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var BrickData_1 = require("../Brick/BrickData");
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var Utility_1 = require("../Common/Utility");
var ColliderAttack_1 = require("./ColliderAttack");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var AIController = /** @class */ (function (_super) {
    __extends(AIController, _super);
    function AIController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampXLeft = -33;
        _this.clampXRight = 33;
        _this.clampYTop = 21;
        _this.clampYBottom = -18;
        _this.speedMove = 20;
        _this.colliderAttack = null;
        _this.brickHom = null;
        _this.brickStick = null;
        _this.brickType = EnumDefine_1.BrickType.BrickAI1;
        _this.stringIncrease = "";
        _this.moveX = 0;
        _this.moveY = 0;
        _this.level = 0;
        _this.listBrickAdd = [];
        _this.x = 0.003;
        _this.lastPositionStickY = 0.013;
        _this.z = -0.002;
        _this.boolCheckDeath = false;
        _this.boolCheckDeathOne = false;
        _this.boolCheckdelay = false;
        _this.boolEnemyRunningIdle = false;
        _this.boolEnemyFollowBox = false;
        _this.boolEnemyAttacking = false;
        return _this;
    }
    //rigidbody: cc.RigidBody3D;
    AIController.prototype.onEnable = function () {
        GamePlayInstance_1.eventDispatcher.on(this.stringIncrease, this.IncreaseStick, this);
    };
    AIController.prototype.onDisable = function () {
        GamePlayInstance_1.eventDispatcher.off(this.stringIncrease, this.IncreaseStick, this);
    };
    // start() {
    //     this.rigidbody = this.node.getComponent(cc.RigidBody3D);
    // }
    AIController.prototype.update = function () {
        //this.CheckingAttack();
        if (!this.boolCheckDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.children[0].children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y));
                var maxDistance = Utility_1.default.Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolCheckDeathOne) {
                            this.EnemyFail();
                            this.boolCheckDeath = true;
                            this.boolCheckDeathOne = true;
                        }
                    }
                }
            }
        }
    };
    AIController.prototype.EnemyFail = function () {
        var _this = this;
        this.unscheduleAllCallbacks();
        this.node.stopAllActions();
        this.colliderAttack.getComponent(ColliderAttack_1.default).unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).play("Fall");
        this.SpawnerBrickWhenFall();
        this.scheduleOnce(function () {
            _this.node.getComponent(cc.SkeletonAnimation).play("Idle");
        }, 1.28);
        this.scheduleOnce(function () {
            _this.boolCheckDeath = false;
            _this.boolCheckDeathOne = false;
            _this.EnemyRunIdle();
        }, 1.6);
    };
    AIController.prototype.StartMove = function () {
        this.EnemyRunIdle();
    };
    AIController.prototype.EnemyRunIdle = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Run");
        this.boolEnemyRunningIdle = true;
        this.boolEnemyFollowBox = false;
        while (Utility_1.default.Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 30) {
            this.moveX = Utility_1.default.RandomRangeFloat(this.clampXLeft, this.clampXRight);
            this.moveY = Utility_1.default.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        }
        this.moveX = Utility_1.default.RandomRangeFloat(this.clampXLeft, this.clampXRight);
        this.moveY = Utility_1.default.RandomRangeFloat(this.clampYBottom, this.clampYTop);
        //let Direction = new cc.Vec2(this.moveX - this.node.x, this.moveY - this.node.y).normalize();
        //this.rigidbody.setLinearVelocity(new cc.Vec3(this.speedMove * Direction.x, this.speedMove * Direction.y, -10));
        //let degree = Utility.CaculatorDegree(Direction);
        //this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, 180, degree)));
        var tween = new cc.Tween().to(2, { position: cc.v3(this.moveX, this.moveY, 0) }).call(function () {
            _this.EnemyRunIdle();
        });
        tween.target(this.node).start();
        //let degree = Utility.CaculatorDegree(cc.v2(this.moveX, this.moveY));
        var degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -degree)));
        this.scheduleOnce(function () {
            _this.node.stopAllActions();
            _this.node.getComponent(cc.SkeletonAnimation).play("Run");
            _this.EnemyRunIdle();
        }, 2.5);
    };
    AIController.prototype.SpawnerBrickWhenFall = function () {
        if (this.level > 0) {
            for (var i = 0; i < this.listBrickAdd.length; i++) {
                var x = Utility_1.default.RandomRangeFloat(-7, 7);
                var y = Utility_1.default.RandomRangeFloat(-7, 7);
                this.SpawnerBrick(x, y);
            }
            this.DecreaseStickAll();
        }
    };
    AIController.prototype.SpawnerBrick = function (x, y) {
        var Brick = cc.instantiate(this.brickHom);
        Brick.parent = cc.Canvas.instance.node;
        Brick.x = this.node.x;
        Brick.y = this.node.y;
        Brick.z = 1;
        var t = new cc.Tween().to(0.3, { position: new cc.Vec3(this.node.x + x, this.node.y + y, 0.5) })
            .target(Brick).start();
    };
    AIController.prototype.IncreaseStick = function () {
        var brick = cc.instantiate(this.brickStick);
        brick.parent = this.node.children[2];
        brick.x = this.x;
        brick.y = this.lastPositionStickY + 0.004;
        brick.z = this.z;
        brick.getComponent(BrickData_1.default).setmaterial(this.brickType);
        this.IncreaseLevel();
        this.lastPositionStickY = this.lastPositionStickY + 0.004;
        this.listBrickAdd.push(brick);
    };
    AIController.prototype.DecreaseStickAll = function () {
        for (var i = 0; i < this.listBrickAdd.length; i++) {
            this.listBrickAdd[i].destroy();
        }
        this.listBrickAdd.splice(0, this.listBrickAdd.length);
        this.lastPositionStickY = 0.013;
        this.node.children[0].scale = 1;
        this.DecreaseLevel();
    };
    AIController.prototype.IncreaseLevel = function () {
        this.node.children[0].scale += 0.1;
        this.level++;
    };
    AIController.prototype.DecreaseLevel = function () {
        this.level = 0;
    };
    AIController.prototype.betweenDegree = function (comVec, dirVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampXLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampXRight", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampYTop", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "clampYBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], AIController.prototype, "speedMove", void 0);
    __decorate([
        property(cc.Node)
    ], AIController.prototype, "colliderAttack", void 0);
    __decorate([
        property(cc.Prefab)
    ], AIController.prototype, "brickHom", void 0);
    __decorate([
        property(cc.Prefab)
    ], AIController.prototype, "brickStick", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.BrickType) })
    ], AIController.prototype, "brickType", void 0);
    __decorate([
        property(cc.String)
    ], AIController.prototype, "stringIncrease", void 0);
    AIController = __decorate([
        ccclass
    ], AIController);
    return AIController;
}(cc.Component));
exports.default = AIController;

cc._RF.pop();