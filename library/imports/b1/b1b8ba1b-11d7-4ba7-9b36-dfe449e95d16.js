"use strict";
cc._RF.push(module, 'b1b8bobEddLp5s23+RJ6V0W', 'ScrollNew');
// Scripts/Common/ScrollNew.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ScrollNew = /** @class */ (function (_super) {
    __extends(ScrollNew, _super);
    function ScrollNew() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.speed = 80;
        _this.resetY = -1138;
        _this.YStart = 1138;
        _this.Bg1 = null;
        _this.Bg2 = null;
        _this.Temp1 = 0;
        _this.Temp2 = 0;
        return _this;
    }
    ScrollNew.prototype.update = function (dt) {
        var y = this.node.y;
        y -= this.speed * dt;
        if (y <= this.resetY) {
            if (this.Bg2.y > this.Bg1.y) {
                this.Bg1.y = this.YStart;
                this.Bg2.y = 0;
            }
            else {
                this.Bg2.y = this.YStart;
                this.Bg1.y = 0;
            }
            y = 0;
        }
        this.node.y = y;
    };
    __decorate([
        property()
    ], ScrollNew.prototype, "speed", void 0);
    __decorate([
        property()
    ], ScrollNew.prototype, "resetY", void 0);
    __decorate([
        property()
    ], ScrollNew.prototype, "YStart", void 0);
    __decorate([
        property(cc.Node)
    ], ScrollNew.prototype, "Bg1", void 0);
    __decorate([
        property(cc.Node)
    ], ScrollNew.prototype, "Bg2", void 0);
    ScrollNew = __decorate([
        ccclass
    ], ScrollNew);
    return ScrollNew;
}(cc.Component));
exports.default = ScrollNew;

cc._RF.pop();