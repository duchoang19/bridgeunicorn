"use strict";
cc._RF.push(module, '92881+lvetMI6WKBsqilrji', 'CameraFollow');
// Scripts/Camera/CameraFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var CharacterController_1 = require("../Controller/CharacterController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraFollow = /** @class */ (function (_super) {
    __extends(CameraFollow, _super);
    function CameraFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cameraOffsetX = 0;
        _this.cameraOffsetY = 0;
        _this.cameraOffsetZ = 0;
        return _this;
    }
    CameraFollow.prototype.start = function () {
        this.cameraOffsetX = this.node.x;
        this.cameraOffsetY = this.node.y + 15;
        this.cameraOffsetZ = this.node.z;
    };
    CameraFollow.prototype.update = function () {
        if (Global_1.default.boolStartPlay) {
            if (!Global_1.default.boolEndGame) {
                if (GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level == 0) {
                    this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                    this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y + this.cameraOffsetY, 0.2);
                    this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.z + this.cameraOffsetZ, 0.2);
                }
                else if (GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level >= 1) {
                    this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                    this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y + this.cameraOffsetY - (1 * GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level), 0.2);
                    this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.z + this.cameraOffsetZ + (0.7 * GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.getComponent(CharacterController_1.default).level), 0.2);
                }
            }
            else {
                this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.myCharacter.z + this.cameraOffsetZ, 0.2);
            }
        }
    };
    CameraFollow = __decorate([
        ccclass
    ], CameraFollow);
    return CameraFollow;
}(cc.Component));
exports.default = CameraFollow;

cc._RF.pop();