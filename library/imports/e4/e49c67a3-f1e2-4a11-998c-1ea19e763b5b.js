"use strict";
cc._RF.push(module, 'e49c6ej8eJKEZmMHqGedjtb', 'KeyEvent');
// Scripts/Common/KeyEvent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyEvent = {
    increaseStickMyCharacter: "increaseStickMyCharacter",
    increaseStickAI1: "increaseStickAI1",
    increaseStickAI2: "increaseStickAI2"
};
exports.default = KeyEvent;

cc._RF.pop();