"use strict";
cc._RF.push(module, '46e6fxcur1NFZbnIGcspwN3', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolCharacterFall: false,
    boolEndGame: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundCollect: null,
    soundClickBtn: null,
    soundBoxBroken: null,
    soundUpStairs: null,
    soundWin: null
};
exports.default = Global;

cc._RF.pop();