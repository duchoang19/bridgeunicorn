import Global from "../Common/Global";
import Utility from "../Common/Utility";
import AIController from "./AIController";
import BoxController from "./BoxController";
import CharacterController from "./CharacterController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ColliderAttack extends cc.Component {
    @property(cc.Node)
    myAI: cc.Node = null;
    start() {
        let collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
    }
    onTriggerEnter(event) {
        if (event.otherCollider.node.group == "Model2") {
            if (event.otherCollider.node.name == "Box" && !this.myAI.getComponent(AIController).boolCheckDeath) {
                this.myAI.getComponent(AIController).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                cc.audioEngine.playEffect(Global.soundAttack, false);
                event.otherCollider.node.getComponent(BoxController).DieWhenEnemyAttack();
                this.scheduleOnce(() => {
                    this.myAI.getComponent(cc.SkeletonAnimation).play("Idle")
                }, 0.708);
                this.scheduleOnce(() => {
                    this.myAI.getComponent(AIController).EnemyRunIdle();
                }, 1.3);
            }
        }
        else if (event.otherCollider.node.group == "Player" && !this.myAI.getComponent(AIController).boolCheckDeath) {
            let Ran = Utility.RandomRangeInteger(1, 5);
            if (Ran == 2) {
                this.myAI.getComponent(AIController).unscheduleAllCallbacks();
                this.myAI.stopAllActions();
                this.myAI.getComponent(cc.SkeletonAnimation).play("Attack");
                event.otherCollider.node.getComponent(CharacterController).CharacterFall();
                cc.audioEngine.playEffect(Global.soundAttack, false);
                this.scheduleOnce(() => {
                    this.myAI.getComponent(cc.SkeletonAnimation).play("Idle")
                }, 0.708);
                this.scheduleOnce(() => {
                    this.myAI.getComponent(AIController).EnemyRunIdle();
                }, 0.85);
            }
        }
    }
}
