import GamePlayController from "../Common/GamePlayController";
import GamePlayInstance from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import CharacterController from "../Controller/CharacterController";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CameraFollow extends cc.Component {
    cameraOffsetX: number = 0;
    cameraOffsetY: number = 0;
    cameraOffsetZ: number = 0;
    start() {
        this.cameraOffsetX = this.node.x;
        this.cameraOffsetY = this.node.y + 15;
        this.cameraOffsetZ = this.node.z;
    }
    update() {
        if (Global.boolStartPlay) {
            if (!Global.boolEndGame) {
                if (GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.getComponent(CharacterController).level == 0) {
                    this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                    this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y + this.cameraOffsetY, 0.2);
                    this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.z + this.cameraOffsetZ, 0.2);
                }
                else if (GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.getComponent(CharacterController).level >= 1) {
                    this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                    this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y + this.cameraOffsetY - (1 * GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.getComponent(CharacterController).level), 0.2);
                    this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.z + this.cameraOffsetZ + (0.7 * GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.getComponent(CharacterController).level), 0.2);
                }
            }
            else {
                this.node.x = cc.misc.lerp(this.node.x, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, GamePlayInstance.Instance(GamePlayInstance).gameplay.myCharacter.z + this.cameraOffsetZ, 0.2);
            }
        }
    }
}
