const { ccclass, property } = cc._decorator;
import { BrickType } from "../Common/EnumDefine";
import GamePlayController from "../Common/GamePlayController";
@ccclass
export default class BrickData extends cc.Component {
    @property(cc.MeshRenderer)
    mesh: cc.MeshRenderer = null;
    setmaterial(materialType: BrickType) {
        let index = 0;
        if (materialType == BrickType.BrickCharacter)
            this.mesh.setMaterial(0, GamePlayController.Instance(GamePlayController).materialBrickCharacter);
        else if (materialType == BrickType.BrickAI1)
            this.mesh.setMaterial(0, GamePlayController.Instance(GamePlayController).materialBrickAI[0]);
        else if (materialType == BrickType.BrickAI2)
            this.mesh.setMaterial(0, GamePlayController.Instance(GamePlayController).materialBrickAI[1]);
    }

}
