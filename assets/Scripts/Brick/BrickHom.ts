import { BrickType } from "../Common/EnumDefine";
import { eventDispatcher } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import AIController from "../Controller/AIController";
import CharacterController from "../Controller/CharacterController";
import BrickData from "./BrickData";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BrickHom extends cc.Component {
    x: number = 0.003;
    z: number = -0.002;
    start() {
        let collider = this.getComponent(cc.Collider3D);
        collider.on('trigger-enter', this.onTriggerEnter, this);
        this.scheduleOnce(() => {
            this.node.getComponent(cc.BoxCollider3D).enabled = true;
        },1);
    }
    onTriggerEnter(event) {
        if (event.otherCollider.node.group == "Player") {
            let otherNode = event.otherCollider.node;
            cc.audioEngine.playEffect(Global.soundCollect, false);
            let pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            let pos = otherNode.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData).setmaterial(otherNode.getComponent(CharacterController).brickType);
            this.node.parent = otherNode.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode.getComponent(CharacterController).lastPositionStickY + 0.004, this.z) })
                .call(() => {
                    eventDispatcher.emit(KeyEvent.increaseStickMyCharacter);
                    this.node.destroy();
                }).target(this.node).start();
        }
        if (event.otherCollider.node.group == "Enemy") {
            let otherNode = event.otherCollider.node;
            cc.audioEngine.playEffect(Global.soundCollect, false);
            let pos1 = cc.Canvas.instance.node.convertToWorldSpaceAR((new cc.Vec3(this.node.x, this.node.y, this.node.z)));
            let pos = otherNode.children[2].convertToNodeSpaceAR(pos1);
            this.node.getComponent(BrickData).setmaterial(otherNode.getComponent(AIController).brickType);
            this.node.parent = otherNode.children[2];
            this.node.x = pos.x;
            this.node.y = pos.y;
            this.node.z = pos.z;
            this.node.scale = 1;
            var t = new cc.Tween();
            t.to(0.2, { scale: 0, position: new cc.Vec3(this.x, otherNode.getComponent(AIController).lastPositionStickY + 0.004, this.z) })
                .call(() => {
                    eventDispatcher.emit(otherNode.getComponent(AIController).stringIncrease);
                    this.node.destroy();
                }).target(this.node).start();
        }
    }
}
