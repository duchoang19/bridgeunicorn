interface KeyEvent {
    increaseStickMyCharacter: string,
    increaseStickAI1: string,
    increaseStickAI2: string,
}
let KeyEvent: KeyEvent =
{
    increaseStickMyCharacter: "increaseStickMyCharacter",
    increaseStickAI1: "increaseStickAI1",
    increaseStickAI2: "increaseStickAI2"
}
export default KeyEvent