import Global from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundManager extends cc.Component {
    @property({
        type: cc.AudioClip
    })
    Bg: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    footStep: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Intro: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    Attack: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    collect: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    boxBroken: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    upStairs: cc.AudioClip = null;
    @property({
        type: cc.AudioClip
    })
    win: cc.AudioClip = null;
    onLoad() {
        Global.soundBG = this.Bg;
        Global.soundIntro = this.Intro;
        Global.soundFootStep = this.footStep;
        Global.soundAttack = this.Attack;
        Global.soundCollect = this.collect;
        Global.soundBoxBroken = this.boxBroken;
        Global.soundUpStairs = this.upStairs;
        Global.soundWin = this.win;
    }
}
