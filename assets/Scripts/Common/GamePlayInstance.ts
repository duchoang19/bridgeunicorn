import BR1 from "../GamePlay/BR1/BR1";
import Singleton from "./Singleton";
const { ccclass, property } = cc._decorator;
export const eventDispatcher = new cc.EventTarget();
@ccclass
export default class GamePlayInstance extends Singleton<GamePlayInstance> {
    gameplay: BR1 = BR1.Instance(BR1);
    constructor() {
        super();
        GamePlayInstance._instance = this;
    }
}
